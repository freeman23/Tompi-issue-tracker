--
-- PostgreSQL database dump
--

-- Dumped from database version 13.2
-- Dumped by pg_dump version 13.2

-- Started on 2021-05-13 00:40:48

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 246 (class 1255 OID 17154)
-- Name: team_member_is_workspace_member_check(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.team_member_is_workspace_member_check() RETURNS trigger
    LANGUAGE plpgsql IMMUTABLE SECURITY DEFINER
    AS $$
declare 
	team_workspace integer;
	is_member bool;
BEGIN
	SELECT workspace_id INTO team_workspace FROM team
	WHERE id = new.team_id;
	SELECT count(*) INTO is_member FROM workspace_member
	WHERE user_id = new.user_id AND workspace_id = team_workspace;
	if is_member THEN
		RETURN NEW;
	ELSE
		RAISE EXCEPTION 'User (%) is not a member of the same workspace (%) as the team (%).', new.user_id, team_workspace, new.team_id;
	END IF;
END;
$$;


ALTER FUNCTION public.team_member_is_workspace_member_check() OWNER TO postgres;

--
-- TOC entry 3341 (class 0 OID 0)
-- Dependencies: 246
-- Name: FUNCTION team_member_is_workspace_member_check(); Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON FUNCTION public.team_member_is_workspace_member_check() IS 'Checks if the user is in the same workspace as the team, before the insertion can happen.
Raises a descriptive error if not.';


--
-- TOC entry 258 (class 1255 OID 17647)
-- Name: typename_referential_purpose_check(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION public.typename_referential_purpose_check() RETURNS trigger
    LANGUAGE plpgsql IMMUTABLE SECURITY DEFINER
    AS $$
declare 
	actual_purpose text;
	typename_id integer;
BEGIN
	CASE TG_ARGV[0]
		WHEN 'role_id' THEN typename_id = NEW.role_id;
		WHEN 'typename_id' THEN typename_id = NEW.typename_id;
		WHEN 'entity_type_id' THEN typename_id = NEW.entity_type_id;
		WHEN 'status_id' THEN typename_id = NEW.status_id;
		ELSE
			RAISE EXCEPTION 'Unknow column name %', TG_ARGV[0];
	END CASE;
	Select purpose into actual_purpose from typename
	Where id = typename_id;
	if TG_ARGV[1] = actual_purpose THEN
		RETURN NEW;
	ELSE
		RAISE EXCEPTION 'Typename % of purpose % is not correct for this column, which expects %', typename_id, actual_purpose, TG_ARGV[1];
	END IF;
END;
$$;


ALTER FUNCTION public.typename_referential_purpose_check() OWNER TO postgres;

--
-- TOC entry 3343 (class 0 OID 0)
-- Dependencies: 258
-- Name: FUNCTION typename_referential_purpose_check(); Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON FUNCTION public.typename_referential_purpose_check() IS 'Checks is the typename used in the proper table depending on the purpose of the typename.
ex.
Checks if  workspace_member role is reffering to a typename purposed with ''Workspace Role''';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- TOC entry 214 (class 1259 OID 16670)
-- Name: color; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.color (
    id integer NOT NULL,
    hex_color character varying(8) NOT NULL,
    CONSTRAINT ck_color CHECK (((hex_color)::text ~ '^[a-fA-F0-9]+$'::text))
);


ALTER TABLE public.color OWNER TO postgres;

--
-- TOC entry 3345 (class 0 OID 0)
-- Dependencies: 214
-- Name: COLUMN color.hex_color; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.color.hex_color IS 'Only hex code, no prefixes.\nAARRGGBB\n\nCHECK: 0-9, A-F';


--
-- TOC entry 213 (class 1259 OID 16668)
-- Name: colors_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.color ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.colors_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 216 (class 1259 OID 16687)
-- Name: issue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.issue (
    id integer NOT NULL,
    title character varying(30) NOT NULL,
    project_id integer NOT NULL,
    type_id integer NOT NULL,
    priority_id integer NOT NULL,
    status_id integer NOT NULL,
    creation_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    due_date timestamp with time zone,
    column_position integer NOT NULL,
    CONSTRAINT ck_column_position CHECK ((column_position >= 0)),
    CONSTRAINT ck_due_date CHECK ((due_date > creation_date))
);


ALTER TABLE public.issue OWNER TO postgres;

--
-- TOC entry 3347 (class 0 OID 0)
-- Dependencies: 216
-- Name: TABLE issue; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.issue IS 'Issues have assignees, resolution, coments, and notifications.';


--
-- TOC entry 3348 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN issue.project_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.issue.project_id IS 'parent project';


--
-- TOC entry 3349 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN issue.type_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.issue.type_id IS 'type of the issue. Task, Bug, etc...';


--
-- TOC entry 3350 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN issue.priority_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.issue.priority_id IS 'priority of the issue';


--
-- TOC entry 3351 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN issue.status_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.issue.status_id IS 'issue status';


--
-- TOC entry 3352 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN issue.due_date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.issue.due_date IS 'Due date, may be null\n\nCHECK: Due date must be after the start date';


--
-- TOC entry 3353 (class 0 OID 0)
-- Dependencies: 216
-- Name: COLUMN issue.column_position; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.issue.column_position IS 'The position of the issue in the column in front end.';


--
-- TOC entry 238 (class 1259 OID 17298)
-- Name: issue_assignee; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.issue_assignee (
    issue_id integer NOT NULL,
    user_id integer NOT NULL,
    team_id integer NOT NULL
);


ALTER TABLE public.issue_assignee OWNER TO postgres;

--
-- TOC entry 237 (class 1259 OID 17283)
-- Name: issue_label; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.issue_label (
    issue_id integer NOT NULL,
    label_id integer NOT NULL
);


ALTER TABLE public.issue_label OWNER TO postgres;

--
-- TOC entry 241 (class 1259 OID 17360)
-- Name: issue_notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.issue_notification (
    notification_id integer NOT NULL,
    issue_id integer NOT NULL
);


ALTER TABLE public.issue_notification OWNER TO postgres;

--
-- TOC entry 240 (class 1259 OID 17340)
-- Name: issue_post; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.issue_post (
    post_id integer NOT NULL,
    issue_id integer NOT NULL,
    replied_to integer
);


ALTER TABLE public.issue_post OWNER TO postgres;

--
-- TOC entry 3358 (class 0 OID 0)
-- Dependencies: 240
-- Name: COLUMN issue_post.replied_to; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.issue_post.replied_to IS 'Issue post id to which this was replied to.';


--
-- TOC entry 239 (class 1259 OID 17313)
-- Name: issue_resolution; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.issue_resolution (
    issue_id integer NOT NULL,
    typename_id integer NOT NULL,
    completion_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.issue_resolution OWNER TO postgres;

--
-- TOC entry 215 (class 1259 OID 16685)
-- Name: issues_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.issue ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.issues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 211 (class 1259 OID 16635)
-- Name: typename; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.typename (
    id integer NOT NULL,
    type_name character varying(25) NOT NULL,
    purpose character varying(35) NOT NULL,
    CONSTRAINT ck_typename CHECK (((type_name)::text ~ '^[a-zA-Z0-9\.\-\_\+\)\(\[\]\{\}\*\&\^\%\$\#\@\!\~\s]+$'::text))
);


ALTER TABLE public.typename OWNER TO postgres;

--
-- TOC entry 3361 (class 0 OID 0)
-- Dependencies: 211
-- Name: TABLE typename; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.typename IS 'typenames are general, and can be used to describe one word entities, ex. types, status, roles, etc...';


--
-- TOC entry 3362 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN typename.type_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.typename.type_name IS 'CHECK: Must be only ascii letters, spaces, and symbols: ~,!,@,#,$,%,^,&,*,(,),_,-,+,{,},[,],.';


--
-- TOC entry 3363 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN typename.purpose; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.typename.purpose IS 'What is this typename used for';


--
-- TOC entry 210 (class 1259 OID 16633)
-- Name: labels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.typename ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.labels_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 203 (class 1259 OID 16526)
-- Name: member; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.member (
    id integer NOT NULL,
    username character varying(35) NOT NULL,
    pass_hash text NOT NULL,
    CONSTRAINT ck_username CHECK (((username)::text ~ '^[a-zA-z][a-zA-Z0-9\.\-\_]+$'::text))
);


ALTER TABLE public.member OWNER TO postgres;

--
-- TOC entry 3365 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN member.username; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.member.username IS 'unique ascii username\n\nCHECK: A combination of letters, digits and (.-_). Must start with a letter';


--
-- TOC entry 3366 (class 0 OID 0)
-- Dependencies: 203
-- Name: COLUMN member.pass_hash; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.member.pass_hash IS 'hashed password, implementation is specified in the application';


--
-- TOC entry 220 (class 1259 OID 16817)
-- Name: milestone; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.milestone (
    id integer NOT NULL,
    project_id integer NOT NULL,
    title character varying(30) NOT NULL,
    overview text NOT NULL,
    completion_date timestamp with time zone,
    creation_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    due_date timestamp with time zone NOT NULL,
    CONSTRAINT ck_due_date CHECK ((due_date > creation_date)),
    CONSTRAINT ck_title CHECK (((title)::text ~ '^[a-zA-z][a-zA-Z0-9\.\-\_]+$'::text))
);


ALTER TABLE public.milestone OWNER TO postgres;

--
-- TOC entry 3368 (class 0 OID 0)
-- Dependencies: 220
-- Name: TABLE milestone; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.milestone IS 'Milestones have associated issues.';


--
-- TOC entry 3369 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN milestone.title; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.milestone.title IS 'Ascii title of the milestones is unique in the project';


--
-- TOC entry 3370 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN milestone.completion_date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.milestone.completion_date IS 'The date of the completion. Null untill completed.';


--
-- TOC entry 3371 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN milestone.due_date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.milestone.due_date IS 'CHECK: Due date is after the start date';


--
-- TOC entry 242 (class 1259 OID 17375)
-- Name: milestone_issue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.milestone_issue (
    milestone_id integer NOT NULL,
    issue_id integer NOT NULL
);


ALTER TABLE public.milestone_issue OWNER TO postgres;

--
-- TOC entry 243 (class 1259 OID 17392)
-- Name: milestone_notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.milestone_notification (
    notification_id integer NOT NULL,
    milestone_id integer NOT NULL
);


ALTER TABLE public.milestone_notification OWNER TO postgres;

--
-- TOC entry 219 (class 1259 OID 16815)
-- Name: milestones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.milestone ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.milestones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 224 (class 1259 OID 16873)
-- Name: notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.notification (
    id integer NOT NULL,
    receiver_id integer NOT NULL,
    sender_id integer NOT NULL,
    creation_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    status_id integer NOT NULL,
    entity_type_id integer NOT NULL
);


ALTER TABLE public.notification OWNER TO postgres;

--
-- TOC entry 3375 (class 0 OID 0)
-- Dependencies: 224
-- Name: TABLE notification; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.notification IS 'A general notification, which can be further specialized.';


--
-- TOC entry 223 (class 1259 OID 16871)
-- Name: notifications_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.notification ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.notifications_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 209 (class 1259 OID 16572)
-- Name: post; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.post (
    id integer NOT NULL,
    author_id integer NOT NULL,
    creation_date timestamp with time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    edit_date timestamp with time zone,
    content text NOT NULL,
    typename_id integer NOT NULL
);


ALTER TABLE public.post OWNER TO postgres;

--
-- TOC entry 3377 (class 0 OID 0)
-- Dependencies: 209
-- Name: TABLE post; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.post IS 'Posts are general, and can be used as pages, comments, etc...\nPosts too have notifications.';


--
-- TOC entry 3378 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN post.author_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.post.author_id IS 'The user who made the post';


--
-- TOC entry 3379 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN post.edit_date; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.post.edit_date IS 'Last edit date';


--
-- TOC entry 3380 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN post.typename_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.post.typename_id IS 'The type of the post, wiki page, issue comment, updates, etc...';


--
-- TOC entry 235 (class 1259 OID 17258)
-- Name: post_attachment; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.post_attachment (
    id integer NOT NULL,
    file_name character varying(100) NOT NULL,
    post_id integer NOT NULL
);


ALTER TABLE public.post_attachment OWNER TO postgres;

--
-- TOC entry 3382 (class 0 OID 0)
-- Dependencies: 235
-- Name: COLUMN post_attachment.file_name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.post_attachment.file_name IS 'Full name of the file. \nThe file is located in the parent workspace folder. Subfolders may be generated depending on the post type.';


--
-- TOC entry 234 (class 1259 OID 17256)
-- Name: post_attachement_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.post_attachment ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.post_attachement_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 236 (class 1259 OID 17268)
-- Name: post_notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.post_notification (
    post_id integer NOT NULL,
    notification_id integer NOT NULL
);


ALTER TABLE public.post_notification OWNER TO postgres;

--
-- TOC entry 208 (class 1259 OID 16570)
-- Name: posts_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.post ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.posts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 207 (class 1259 OID 16559)
-- Name: project; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project (
    id integer NOT NULL,
    name character varying(35) NOT NULL,
    workspace_id integer NOT NULL,
    CONSTRAINT ck_name CHECK (((name)::text ~ '^[a-zA-z][a-zA-Z0-9\.\-\_]+$'::text))
);


ALTER TABLE public.project OWNER TO postgres;

--
-- TOC entry 3385 (class 0 OID 0)
-- Dependencies: 207
-- Name: TABLE project; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.project IS 'Projects have wikies, issues, milestones and norifications.';


--
-- TOC entry 3386 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN project.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.project.name IS 'The ascii name is unique within the given workspace.';


--
-- TOC entry 3387 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN project.workspace_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.project.workspace_id IS 'The workspace to which the project belongs to';


--
-- TOC entry 217 (class 1259 OID 16756)
-- Name: project_issue_priority_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_issue_priority_type (
    typename_id integer NOT NULL,
    project_id integer NOT NULL,
    color_id integer NOT NULL
);


ALTER TABLE public.project_issue_priority_type OWNER TO postgres;

--
-- TOC entry 3389 (class 0 OID 0)
-- Dependencies: 217
-- Name: TABLE project_issue_priority_type; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.project_issue_priority_type IS 'Priorities used in the project';


--
-- TOC entry 218 (class 1259 OID 16776)
-- Name: project_issue_status; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_issue_status (
    typename_id integer NOT NULL,
    project_id integer NOT NULL,
    color_id integer NOT NULL
);


ALTER TABLE public.project_issue_status OWNER TO postgres;

--
-- TOC entry 3391 (class 0 OID 0)
-- Dependencies: 218
-- Name: TABLE project_issue_status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.project_issue_status IS 'Issue statuses used in the project';


--
-- TOC entry 212 (class 1259 OID 16642)
-- Name: project_issue_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_issue_type (
    typename_id integer NOT NULL,
    project_id integer NOT NULL,
    color_id integer NOT NULL
);


ALTER TABLE public.project_issue_type OWNER TO postgres;

--
-- TOC entry 3393 (class 0 OID 0)
-- Dependencies: 212
-- Name: TABLE project_issue_type; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.project_issue_type IS 'Issue types used in project';


--
-- TOC entry 232 (class 1259 OID 17215)
-- Name: project_notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_notification (
    notification_id integer NOT NULL,
    project_id integer NOT NULL
);


ALTER TABLE public.project_notification OWNER TO postgres;

--
-- TOC entry 233 (class 1259 OID 17230)
-- Name: project_post; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_post (
    post_id integer NOT NULL,
    project_id integer NOT NULL
);


ALTER TABLE public.project_post OWNER TO postgres;

--
-- TOC entry 221 (class 1259 OID 16835)
-- Name: project_tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_tag (
    typename_id integer NOT NULL,
    project_id integer NOT NULL,
    color_id integer NOT NULL
);


ALTER TABLE public.project_tag OWNER TO postgres;

--
-- TOC entry 244 (class 1259 OID 17454)
-- Name: project_team; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.project_team (
    project_id integer NOT NULL,
    team_id integer NOT NULL
);


ALTER TABLE public.project_team OWNER TO postgres;

--
-- TOC entry 3398 (class 0 OID 0)
-- Dependencies: 244
-- Name: TABLE project_team; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.project_team IS 'Teams invited to the project';


--
-- TOC entry 206 (class 1259 OID 16557)
-- Name: projects_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.project ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 205 (class 1259 OID 16538)
-- Name: team; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.team (
    id integer NOT NULL,
    name character varying(30) NOT NULL,
    workspace_id integer NOT NULL,
    info text DEFAULT 'Placeholder'::text NOT NULL,
    logo character varying(100) DEFAULT 'default_team_logo.jpg'::character varying NOT NULL,
    CONSTRAINT ck_name CHECK (((name)::text ~ '^[a-zA-z][a-zA-Z0-9\.]+$'::text))
);


ALTER TABLE public.team OWNER TO postgres;

--
-- TOC entry 3400 (class 0 OID 0)
-- Dependencies: 205
-- Name: TABLE team; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.team IS 'Teams have unique names within a workspace, they have members, and notifications';


--
-- TOC entry 3401 (class 0 OID 0)
-- Dependencies: 205
-- Name: COLUMN team.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.team.name IS 'workspace specific unique ascii name.\n\nCHECK: The team name must be in ascii and start with a letter.';


--
-- TOC entry 3402 (class 0 OID 0)
-- Dependencies: 205
-- Name: COLUMN team.workspace_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.team.workspace_id IS 'The workspace to which this belongs to';


--
-- TOC entry 3403 (class 0 OID 0)
-- Dependencies: 205
-- Name: COLUMN team.info; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.team.info IS 'A general description about the team';


--
-- TOC entry 3404 (class 0 OID 0)
-- Dependencies: 205
-- Name: COLUMN team.logo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.team.logo IS 'Filename of the logo';


--
-- TOC entry 228 (class 1259 OID 17123)
-- Name: team_member; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.team_member (
    user_id integer NOT NULL,
    team_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.team_member OWNER TO postgres;

--
-- TOC entry 3406 (class 0 OID 0)
-- Dependencies: 228
-- Name: TABLE team_member; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.team_member IS 'Members of the team.\nTo be a member of a team from X workspace, a user must be a member of X workspace first. \nA Insert trigger does the check.';


--
-- TOC entry 229 (class 1259 OID 17157)
-- Name: team_notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.team_notification (
    notification_id integer NOT NULL,
    team_id integer NOT NULL
);


ALTER TABLE public.team_notification OWNER TO postgres;

--
-- TOC entry 3408 (class 0 OID 0)
-- Dependencies: 229
-- Name: TABLE team_notification; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.team_notification IS 'Notifications related to the team.';


--
-- TOC entry 230 (class 1259 OID 17183)
-- Name: team_post; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.team_post (
    post_id integer NOT NULL,
    team_id integer NOT NULL
);


ALTER TABLE public.team_post OWNER TO postgres;

--
-- TOC entry 204 (class 1259 OID 16536)
-- Name: teams_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.team ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.teams_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 245 (class 1259 OID 17651)
-- Name: typename_purpose; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.typename_purpose (
    purpose character varying(35) NOT NULL
);


ALTER TABLE public.typename_purpose OWNER TO postgres;

--
-- TOC entry 3411 (class 0 OID 0)
-- Dependencies: 245
-- Name: TABLE typename_purpose; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.typename_purpose IS 'Enumeration, for the typename table, to specify the purpose the type is used for.';


--
-- TOC entry 202 (class 1259 OID 16524)
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.member ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 222 (class 1259 OID 16855)
-- Name: wiki_page; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wiki_page (
    post_id integer NOT NULL,
    project_id integer NOT NULL,
    title character varying(30) NOT NULL,
    CONSTRAINT ck_title CHECK (((title)::text ~ '^[a-zA-z][a-zA-Z0-9]+$'::text))
);


ALTER TABLE public.wiki_page OWNER TO postgres;

--
-- TOC entry 3413 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN wiki_page.post_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.wiki_page.post_id IS 'The page is actually a post, with some additions';


--
-- TOC entry 3414 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN wiki_page.title; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.wiki_page.title IS 'CHECK: title must be in ascii';


--
-- TOC entry 231 (class 1259 OID 17200)
-- Name: wiki_page_tag; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.wiki_page_tag (
    page_id integer NOT NULL,
    tag_id integer NOT NULL
);


ALTER TABLE public.wiki_page_tag OWNER TO postgres;

--
-- TOC entry 201 (class 1259 OID 16475)
-- Name: workspace; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.workspace (
    id integer NOT NULL,
    name character varying(30) NOT NULL,
    info text DEFAULT 'Placeholder'::text NOT NULL,
    logo character varying(100) DEFAULT 'default_logo.jpg'::character varying NOT NULL,
    CONSTRAINT ck_name CHECK (((name)::text ~ '^[a-zA-z][a-zA-Z0-9\.\-\_]+$'::text))
);


ALTER TABLE public.workspace OWNER TO postgres;

--
-- TOC entry 3417 (class 0 OID 0)
-- Dependencies: 201
-- Name: TABLE workspace; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.workspace IS 'Workspaces have unique names, members, projects, posts, teams, notifications.';


--
-- TOC entry 3418 (class 0 OID 0)
-- Dependencies: 201
-- Name: COLUMN workspace.name; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.workspace.name IS 'Unique workspace name.\nThe name is also the path of the workspace related files.\n\nCHECK: A combination of letters, digits and (.-_). Must start with a letter';


--
-- TOC entry 3419 (class 0 OID 0)
-- Dependencies: 201
-- Name: COLUMN workspace.info; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.workspace.info IS 'A general information about the workspace';


--
-- TOC entry 3420 (class 0 OID 0)
-- Dependencies: 201
-- Name: COLUMN workspace.logo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.workspace.logo IS 'Filename of the logo';


--
-- TOC entry 225 (class 1259 OID 16969)
-- Name: workspace_member; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.workspace_member (
    workspace_id integer NOT NULL,
    user_id integer NOT NULL,
    role_id integer NOT NULL
);


ALTER TABLE public.workspace_member OWNER TO postgres;

--
-- TOC entry 3422 (class 0 OID 0)
-- Dependencies: 225
-- Name: COLUMN workspace_member.role_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN public.workspace_member.role_id IS 'The role in the workspace';


--
-- TOC entry 226 (class 1259 OID 17060)
-- Name: workspace_notification; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.workspace_notification (
    notification_id integer NOT NULL,
    workspace_id integer NOT NULL
);


ALTER TABLE public.workspace_notification OWNER TO postgres;

--
-- TOC entry 3424 (class 0 OID 0)
-- Dependencies: 226
-- Name: TABLE workspace_notification; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.workspace_notification IS 'Notifications related to the workspace';


--
-- TOC entry 227 (class 1259 OID 17075)
-- Name: workspace_post; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.workspace_post (
    workspace_id integer NOT NULL,
    post_id integer NOT NULL
);


ALTER TABLE public.workspace_post OWNER TO postgres;

--
-- TOC entry 3426 (class 0 OID 0)
-- Dependencies: 227
-- Name: TABLE workspace_post; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE public.workspace_post IS 'Posts related only to the workspace';


--
-- TOC entry 200 (class 1259 OID 16473)
-- Name: workspaces_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

ALTER TABLE public.workspace ALTER COLUMN id ADD GENERATED ALWAYS AS IDENTITY (
    SEQUENCE NAME public.workspaces_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);


--
-- TOC entry 3058 (class 2606 OID 16674)
-- Name: color pk_colors_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.color
    ADD CONSTRAINT pk_colors_id PRIMARY KEY (id);


--
-- TOC entry 3112 (class 2606 OID 17412)
-- Name: issue_assignee pk_issue_assignees_issue_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_assignee
    ADD CONSTRAINT pk_issue_assignees_issue_id PRIMARY KEY (issue_id, user_id);


--
-- TOC entry 3110 (class 2606 OID 17287)
-- Name: issue_label pk_issue_labels_issue_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_label
    ADD CONSTRAINT pk_issue_labels_issue_id PRIMARY KEY (issue_id, label_id);


--
-- TOC entry 3118 (class 2606 OID 17364)
-- Name: issue_notification pk_issue_notifications_notification_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_notification
    ADD CONSTRAINT pk_issue_notifications_notification_id PRIMARY KEY (notification_id);


--
-- TOC entry 3116 (class 2606 OID 17344)
-- Name: issue_post pk_issue_posts_post_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_post
    ADD CONSTRAINT pk_issue_posts_post_id PRIMARY KEY (post_id);


--
-- TOC entry 3114 (class 2606 OID 17318)
-- Name: issue_resolution pk_issue_resolution_issue_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_resolution
    ADD CONSTRAINT pk_issue_resolution_issue_id PRIMARY KEY (issue_id);


--
-- TOC entry 3062 (class 2606 OID 16692)
-- Name: issue pk_issues_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue
    ADD CONSTRAINT pk_issues_id PRIMARY KEY (id);


--
-- TOC entry 3120 (class 2606 OID 17391)
-- Name: milestone_issue pk_milestone_issues_milestone_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.milestone_issue
    ADD CONSTRAINT pk_milestone_issues_milestone_id PRIMARY KEY (milestone_id, issue_id);


--
-- TOC entry 3122 (class 2606 OID 17396)
-- Name: milestone_notification pk_milestone_notification_notification_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.milestone_notification
    ADD CONSTRAINT pk_milestone_notification_notification_id PRIMARY KEY (notification_id);


--
-- TOC entry 3074 (class 2606 OID 16825)
-- Name: milestone pk_milestones_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.milestone
    ADD CONSTRAINT pk_milestones_id PRIMARY KEY (id);


--
-- TOC entry 3084 (class 2606 OID 16877)
-- Name: notification pk_notifications_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT pk_notifications_id PRIMARY KEY (id);


--
-- TOC entry 3106 (class 2606 OID 17262)
-- Name: post_attachment pk_post_attachement_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post_attachment
    ADD CONSTRAINT pk_post_attachement_id PRIMARY KEY (id);


--
-- TOC entry 3108 (class 2606 OID 17272)
-- Name: post_notification pk_post_notifications_post_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post_notification
    ADD CONSTRAINT pk_post_notifications_post_id PRIMARY KEY (notification_id);


--
-- TOC entry 3047 (class 2606 OID 16581)
-- Name: post pk_posts_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT pk_posts_id PRIMARY KEY (id);


--
-- TOC entry 3070 (class 2606 OID 17414)
-- Name: project_issue_status pk_project_issue_statuses_label_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_status
    ADD CONSTRAINT pk_project_issue_statuses_label_id PRIMARY KEY (typename_id, project_id);


--
-- TOC entry 3054 (class 2606 OID 17423)
-- Name: project_issue_type pk_project_issue_types_label_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_type
    ADD CONSTRAINT pk_project_issue_types_label_id PRIMARY KEY (typename_id, project_id);


--
-- TOC entry 3102 (class 2606 OID 17253)
-- Name: project_notification pk_project_notifications_notification_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_notification
    ADD CONSTRAINT pk_project_notifications_notification_id PRIMARY KEY (notification_id);


--
-- TOC entry 3104 (class 2606 OID 17234)
-- Name: project_post pk_project_posts_post_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_post
    ADD CONSTRAINT pk_project_posts_post_id PRIMARY KEY (post_id);


--
-- TOC entry 3066 (class 2606 OID 17432)
-- Name: project_issue_priority_type pk_project_priority_types_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_priority_type
    ADD CONSTRAINT pk_project_priority_types_id PRIMARY KEY (typename_id, project_id);


--
-- TOC entry 3124 (class 2606 OID 17458)
-- Name: project_team pk_project_teams_project_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_team
    ADD CONSTRAINT pk_project_teams_project_id PRIMARY KEY (project_id, team_id);


--
-- TOC entry 3044 (class 2606 OID 16563)
-- Name: project pk_projects_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT pk_projects_id PRIMARY KEY (id);


--
-- TOC entry 3094 (class 2606 OID 17127)
-- Name: team_member pk_team_members; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team_member
    ADD CONSTRAINT pk_team_members PRIMARY KEY (user_id, team_id);


--
-- TOC entry 3096 (class 2606 OID 17255)
-- Name: team_notification pk_team_notifications_notification_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team_notification
    ADD CONSTRAINT pk_team_notifications_notification_id PRIMARY KEY (notification_id);


--
-- TOC entry 3098 (class 2606 OID 17249)
-- Name: team_post pk_team_posts_post_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team_post
    ADD CONSTRAINT pk_team_posts_post_id PRIMARY KEY (post_id);


--
-- TOC entry 3041 (class 2606 OID 16547)
-- Name: team pk_teams_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team
    ADD CONSTRAINT pk_teams_id PRIMARY KEY (id);


--
-- TOC entry 3126 (class 2606 OID 17655)
-- Name: typename_purpose pk_typename_purpose_purpose; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.typename_purpose
    ADD CONSTRAINT pk_typename_purpose_purpose PRIMARY KEY (purpose);


--
-- TOC entry 3049 (class 2606 OID 16639)
-- Name: typename pk_typenames_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.typename
    ADD CONSTRAINT pk_typenames_id PRIMARY KEY (id);


--
-- TOC entry 3038 (class 2606 OID 16533)
-- Name: member pk_users_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.member
    ADD CONSTRAINT pk_users_id PRIMARY KEY (id);


--
-- TOC entry 3100 (class 2606 OID 17204)
-- Name: wiki_page_tag pk_wiki_page_tags_page_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wiki_page_tag
    ADD CONSTRAINT pk_wiki_page_tags_page_id PRIMARY KEY (page_id, tag_id);


--
-- TOC entry 3082 (class 2606 OID 16859)
-- Name: wiki_page pk_wiki_pages_post_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wiki_page
    ADD CONSTRAINT pk_wiki_pages_post_id PRIMARY KEY (post_id);


--
-- TOC entry 3078 (class 2606 OID 17441)
-- Name: project_tag pk_wiki_tags_label_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_tag
    ADD CONSTRAINT pk_wiki_tags_label_id PRIMARY KEY (typename_id, project_id);


--
-- TOC entry 3086 (class 2606 OID 16973)
-- Name: workspace_member pk_workspace_members_workspace_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workspace_member
    ADD CONSTRAINT pk_workspace_members_workspace_id PRIMARY KEY (workspace_id, user_id);


--
-- TOC entry 3090 (class 2606 OID 17251)
-- Name: workspace_notification pk_workspace_notifications; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workspace_notification
    ADD CONSTRAINT pk_workspace_notifications PRIMARY KEY (notification_id);


--
-- TOC entry 3092 (class 2606 OID 17247)
-- Name: workspace_post pk_workspace_posts_post_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workspace_post
    ADD CONSTRAINT pk_workspace_posts_post_id PRIMARY KEY (post_id);


--
-- TOC entry 3035 (class 2606 OID 16484)
-- Name: workspace pk_workspaces_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workspace
    ADD CONSTRAINT pk_workspaces_id PRIMARY KEY (id);


--
-- TOC entry 3060 (class 2606 OID 16703)
-- Name: color unq_colors_hex; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.color
    ADD CONSTRAINT unq_colors_hex UNIQUE (hex_color);


--
-- TOC entry 3064 (class 2606 OID 16694)
-- Name: issue unq_issues_column_position; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue
    ADD CONSTRAINT unq_issues_column_position UNIQUE (column_position);


--
-- TOC entry 3076 (class 2606 OID 16827)
-- Name: milestone unq_milestones; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.milestone
    ADD CONSTRAINT unq_milestones UNIQUE (project_id, title);


--
-- TOC entry 3072 (class 2606 OID 17416)
-- Name: project_issue_status unq_project_issue_status_typename_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_status
    ADD CONSTRAINT unq_project_issue_status_typename_id UNIQUE (typename_id);


--
-- TOC entry 3056 (class 2606 OID 17425)
-- Name: project_issue_type unq_project_issue_types_label_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_type
    ADD CONSTRAINT unq_project_issue_types_label_id UNIQUE (typename_id);


--
-- TOC entry 3068 (class 2606 OID 17434)
-- Name: project_issue_priority_type unq_project_priority_types_typename_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_priority_type
    ADD CONSTRAINT unq_project_priority_types_typename_id UNIQUE (typename_id);


--
-- TOC entry 3080 (class 2606 OID 17443)
-- Name: project_tag unq_project_tags_typename_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_tag
    ADD CONSTRAINT unq_project_tags_typename_id UNIQUE (typename_id);


--
-- TOC entry 3052 (class 2606 OID 17657)
-- Name: typename unq_typenames_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.typename
    ADD CONSTRAINT unq_typenames_id UNIQUE (id, purpose);


--
-- TOC entry 3088 (class 2606 OID 17638)
-- Name: workspace_member unq_workspace_member_user_id; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workspace_member
    ADD CONSTRAINT unq_workspace_member_user_id UNIQUE (user_id);


--
-- TOC entry 3045 (class 1259 OID 16564)
-- Name: unq_projects; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unq_projects ON public.project USING btree (name, workspace_id);


--
-- TOC entry 3042 (class 1259 OID 16549)
-- Name: unq_teams_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unq_teams_name ON public.team USING btree (name, workspace_id);


--
-- TOC entry 3050 (class 1259 OID 17658)
-- Name: unq_typenames; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unq_typenames ON public.typename USING btree (type_name, purpose);


--
-- TOC entry 3039 (class 1259 OID 16535)
-- Name: unq_users_username; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unq_users_username ON public.member USING btree (username);


--
-- TOC entry 3036 (class 1259 OID 16486)
-- Name: unq_workspaces_name; Type: INDEX; Schema: public; Owner: postgres
--

CREATE UNIQUE INDEX unq_workspaces_name ON public.workspace USING btree (name);


--
-- TOC entry 3204 (class 2620 OID 17156)
-- Name: team_member check_user_membership; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE CONSTRAINT TRIGGER check_user_membership AFTER INSERT ON public.team_member NOT DEFERRABLE INITIALLY IMMEDIATE FOR EACH ROW EXECUTE FUNCTION public.team_member_is_workspace_member_check();


--
-- TOC entry 3428 (class 0 OID 0)
-- Dependencies: 3204
-- Name: TRIGGER check_user_membership ON team_member; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TRIGGER check_user_membership ON public.team_member IS 'Checks if the user is a member of the team workspace';


--
-- TOC entry 3205 (class 2620 OID 17677)
-- Name: issue_resolution validate_typename_issue_resolution; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER validate_typename_issue_resolution BEFORE INSERT OR UPDATE OF typename_id ON public.issue_resolution FOR EACH ROW EXECUTE FUNCTION public.typename_referential_purpose_check('typename_id', 'Issue Resolution Type');


--
-- TOC entry 3429 (class 0 OID 0)
-- Dependencies: 3205
-- Name: TRIGGER validate_typename_issue_resolution ON issue_resolution; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TRIGGER validate_typename_issue_resolution ON public.issue_resolution IS 'Ensures that the typename purpose matches the usage';


--
-- TOC entry 3201 (class 2620 OID 17681)
-- Name: notification validate_typename_notification_status; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER validate_typename_notification_status BEFORE INSERT OR UPDATE OF status_id ON public.notification FOR EACH ROW EXECUTE FUNCTION public.typename_referential_purpose_check('entity_type_id', 'Notification Status');


--
-- TOC entry 3430 (class 0 OID 0)
-- Dependencies: 3201
-- Name: TRIGGER validate_typename_notification_status ON notification; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TRIGGER validate_typename_notification_status ON public.notification IS 'Ensures that the typename purpose matches the usage';


--
-- TOC entry 3200 (class 2620 OID 17678)
-- Name: notification validate_typename_notification_type; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER validate_typename_notification_type BEFORE INSERT OR UPDATE OF entity_type_id ON public.notification FOR EACH ROW EXECUTE FUNCTION public.typename_referential_purpose_check('entity_type_id', 'Notification Entitiy');


--
-- TOC entry 3431 (class 0 OID 0)
-- Dependencies: 3200
-- Name: TRIGGER validate_typename_notification_type ON notification; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TRIGGER validate_typename_notification_type ON public.notification IS 'Ensures that the typename purpose matches the usage';


--
-- TOC entry 3197 (class 2620 OID 17683)
-- Name: project_issue_priority_type validate_typename_pi_priority; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER validate_typename_pi_priority BEFORE INSERT OR UPDATE OF typename_id ON public.project_issue_priority_type FOR EACH ROW EXECUTE FUNCTION public.typename_referential_purpose_check('typename_id', 'Project Issue Priority');


--
-- TOC entry 3432 (class 0 OID 0)
-- Dependencies: 3197
-- Name: TRIGGER validate_typename_pi_priority ON project_issue_priority_type; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TRIGGER validate_typename_pi_priority ON public.project_issue_priority_type IS 'Ensures that the typename purpose matches the usage';


--
-- TOC entry 3198 (class 2620 OID 17684)
-- Name: project_issue_status validate_typename_pi_status; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER validate_typename_pi_status BEFORE INSERT OR UPDATE OF typename_id ON public.project_issue_status FOR EACH ROW EXECUTE FUNCTION public.typename_referential_purpose_check('typename_id', 'Project Issue Status');


--
-- TOC entry 3433 (class 0 OID 0)
-- Dependencies: 3198
-- Name: TRIGGER validate_typename_pi_status ON project_issue_status; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TRIGGER validate_typename_pi_status ON public.project_issue_status IS 'Ensures that the typename purpose matches the usage';


--
-- TOC entry 3196 (class 2620 OID 17685)
-- Name: project_issue_type validate_typename_pi_type; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER validate_typename_pi_type BEFORE INSERT OR UPDATE OF typename_id ON public.project_issue_type FOR EACH ROW EXECUTE FUNCTION public.typename_referential_purpose_check('typename_id', 'Project Issue Type');


--
-- TOC entry 3434 (class 0 OID 0)
-- Dependencies: 3196
-- Name: TRIGGER validate_typename_pi_type ON project_issue_type; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TRIGGER validate_typename_pi_type ON public.project_issue_type IS 'Ensures that the typename purpose matches the usage';


--
-- TOC entry 3195 (class 2620 OID 17682)
-- Name: post validate_typename_post_type; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER validate_typename_post_type BEFORE INSERT OR UPDATE OF typename_id ON public.post FOR EACH ROW EXECUTE FUNCTION public.typename_referential_purpose_check('typename_id', 'Post Type');


--
-- TOC entry 3435 (class 0 OID 0)
-- Dependencies: 3195
-- Name: TRIGGER validate_typename_post_type ON post; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TRIGGER validate_typename_post_type ON public.post IS 'Ensures that the typename purpose matches the usage';


--
-- TOC entry 3199 (class 2620 OID 17686)
-- Name: project_tag validate_typename_project_tag; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER validate_typename_project_tag BEFORE INSERT OR UPDATE OF typename_id ON public.project_tag FOR EACH ROW EXECUTE FUNCTION public.typename_referential_purpose_check('typename_id', 'Project Tag');


--
-- TOC entry 3436 (class 0 OID 0)
-- Dependencies: 3199
-- Name: TRIGGER validate_typename_project_tag ON project_tag; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TRIGGER validate_typename_project_tag ON public.project_tag IS 'Ensures that the typename purpose matches the usage';


--
-- TOC entry 3203 (class 2620 OID 17650)
-- Name: team_member validate_typename_team_role; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER validate_typename_team_role BEFORE INSERT OR UPDATE OF role_id ON public.team_member FOR EACH ROW EXECUTE FUNCTION public.typename_referential_purpose_check('role_id', 'Team Role');


--
-- TOC entry 3437 (class 0 OID 0)
-- Dependencies: 3203
-- Name: TRIGGER validate_typename_team_role ON team_member; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TRIGGER validate_typename_team_role ON public.team_member IS 'Ensures that the typename purpose matches the usage';


--
-- TOC entry 3202 (class 2620 OID 17649)
-- Name: workspace_member validate_typename_workspace_role; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER validate_typename_workspace_role BEFORE INSERT OR UPDATE OF role_id ON public.workspace_member FOR EACH ROW EXECUTE FUNCTION public.typename_referential_purpose_check('role_id', 'Workspace Role');


--
-- TOC entry 3438 (class 0 OID 0)
-- Dependencies: 3202
-- Name: TRIGGER validate_typename_workspace_role ON workspace_member; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TRIGGER validate_typename_workspace_role ON public.workspace_member IS 'Ensures that the typename purpose matches the usage';


--
-- TOC entry 3181 (class 2606 OID 17308)
-- Name: issue_assignee fk_issue_assignees; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_assignee
    ADD CONSTRAINT fk_issue_assignees FOREIGN KEY (user_id, team_id) REFERENCES public.team_member(user_id, team_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3180 (class 2606 OID 17303)
-- Name: issue_assignee fk_issue_assignees_issues; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_assignee
    ADD CONSTRAINT fk_issue_assignees_issues FOREIGN KEY (issue_id) REFERENCES public.issue(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3178 (class 2606 OID 17293)
-- Name: issue_label fk_issue_labels_issues; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_label
    ADD CONSTRAINT fk_issue_labels_issues FOREIGN KEY (issue_id) REFERENCES public.issue(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3179 (class 2606 OID 17449)
-- Name: issue_label fk_issue_labels_project_tags; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_label
    ADD CONSTRAINT fk_issue_labels_project_tags FOREIGN KEY (label_id) REFERENCES public.project_tag(typename_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3188 (class 2606 OID 17365)
-- Name: issue_notification fk_issue_notifications; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_notification
    ADD CONSTRAINT fk_issue_notifications FOREIGN KEY (notification_id) REFERENCES public.notification(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3187 (class 2606 OID 17370)
-- Name: issue_notification fk_issue_notifications_issues; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_notification
    ADD CONSTRAINT fk_issue_notifications_issues FOREIGN KEY (issue_id) REFERENCES public.issue(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3186 (class 2606 OID 17711)
-- Name: issue_post fk_issue_post_issue_post; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_post
    ADD CONSTRAINT fk_issue_post_issue_post FOREIGN KEY (replied_to) REFERENCES public.issue_post(post_id) ON UPDATE CASCADE ON DELETE SET NULL;


--
-- TOC entry 3184 (class 2606 OID 17345)
-- Name: issue_post fk_issue_posts_issues; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_post
    ADD CONSTRAINT fk_issue_posts_issues FOREIGN KEY (issue_id) REFERENCES public.issue(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3185 (class 2606 OID 17350)
-- Name: issue_post fk_issue_posts_posts; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_post
    ADD CONSTRAINT fk_issue_posts_posts FOREIGN KEY (post_id) REFERENCES public.post(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3182 (class 2606 OID 17619)
-- Name: issue_resolution fk_issue_resolution; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_resolution
    ADD CONSTRAINT fk_issue_resolution FOREIGN KEY (typename_id) REFERENCES public.typename(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3183 (class 2606 OID 17319)
-- Name: issue_resolution fk_issue_resolution_issues; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue_resolution
    ADD CONSTRAINT fk_issue_resolution_issues FOREIGN KEY (issue_id) REFERENCES public.issue(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3137 (class 2606 OID 17435)
-- Name: issue fk_issues_priority; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue
    ADD CONSTRAINT fk_issues_priority FOREIGN KEY (priority_id) REFERENCES public.project_issue_priority_type(typename_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3135 (class 2606 OID 17417)
-- Name: issue fk_issues_project_issue_status; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue
    ADD CONSTRAINT fk_issues_project_issue_status FOREIGN KEY (status_id) REFERENCES public.project_issue_status(typename_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3136 (class 2606 OID 17426)
-- Name: issue fk_issues_project_issue_types; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue
    ADD CONSTRAINT fk_issues_project_issue_types FOREIGN KEY (type_id) REFERENCES public.project_issue_type(typename_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3138 (class 2606 OID 16696)
-- Name: issue fk_issues_projects; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.issue
    ADD CONSTRAINT fk_issues_projects FOREIGN KEY (project_id) REFERENCES public.project(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3190 (class 2606 OID 17385)
-- Name: milestone_issue fk_milestone_issues_issues; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.milestone_issue
    ADD CONSTRAINT fk_milestone_issues_issues FOREIGN KEY (issue_id) REFERENCES public.issue(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3189 (class 2606 OID 17380)
-- Name: milestone_issue fk_milestone_issues_milestones; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.milestone_issue
    ADD CONSTRAINT fk_milestone_issues_milestones FOREIGN KEY (milestone_id) REFERENCES public.milestone(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3191 (class 2606 OID 17397)
-- Name: milestone_notification fk_milestone_notification_ml; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.milestone_notification
    ADD CONSTRAINT fk_milestone_notification_ml FOREIGN KEY (milestone_id) REFERENCES public.milestone(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3192 (class 2606 OID 17402)
-- Name: milestone_notification fk_milestone_notification_nf; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.milestone_notification
    ADD CONSTRAINT fk_milestone_notification_nf FOREIGN KEY (notification_id) REFERENCES public.notification(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3145 (class 2606 OID 16829)
-- Name: milestone fk_milestones_projects; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.milestone
    ADD CONSTRAINT fk_milestones_projects FOREIGN KEY (project_id) REFERENCES public.project(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3151 (class 2606 OID 17609)
-- Name: notification fk_notifications_entity_type; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fk_notifications_entity_type FOREIGN KEY (entity_type_id) REFERENCES public.typename(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3152 (class 2606 OID 17614)
-- Name: notification fk_notifications_status; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fk_notifications_status FOREIGN KEY (status_id) REFERENCES public.typename(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3153 (class 2606 OID 16894)
-- Name: notification fk_notifications_users_reciever; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fk_notifications_users_reciever FOREIGN KEY (receiver_id) REFERENCES public.member(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3154 (class 2606 OID 16899)
-- Name: notification fk_notifications_users_sender; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.notification
    ADD CONSTRAINT fk_notifications_users_sender FOREIGN KEY (sender_id) REFERENCES public.member(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3175 (class 2606 OID 17263)
-- Name: post_attachment fk_post_attachement_posts; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post_attachment
    ADD CONSTRAINT fk_post_attachement_posts FOREIGN KEY (post_id) REFERENCES public.post(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3176 (class 2606 OID 17273)
-- Name: post_notification fk_post_notifications; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post_notification
    ADD CONSTRAINT fk_post_notifications FOREIGN KEY (notification_id) REFERENCES public.notification(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3177 (class 2606 OID 17278)
-- Name: post_notification fk_post_notifications_posts; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post_notification
    ADD CONSTRAINT fk_post_notifications_posts FOREIGN KEY (post_id) REFERENCES public.post(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3130 (class 2606 OID 17604)
-- Name: post fk_posts_post_type; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT fk_posts_post_type FOREIGN KEY (typename_id) REFERENCES public.typename(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3129 (class 2606 OID 16587)
-- Name: post fk_posts_users; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.post
    ADD CONSTRAINT fk_posts_users FOREIGN KEY (author_id) REFERENCES public.member(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3144 (class 2606 OID 16791)
-- Name: project_issue_status fk_project_issue_statuses_colors; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_status
    ADD CONSTRAINT fk_project_issue_statuses_colors FOREIGN KEY (color_id) REFERENCES public.color(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3142 (class 2606 OID 16781)
-- Name: project_issue_status fk_project_issue_statuses_labels; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_status
    ADD CONSTRAINT fk_project_issue_statuses_labels FOREIGN KEY (typename_id) REFERENCES public.typename(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3143 (class 2606 OID 16786)
-- Name: project_issue_status fk_project_issue_statuses_projects; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_status
    ADD CONSTRAINT fk_project_issue_statuses_projects FOREIGN KEY (project_id) REFERENCES public.project(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3134 (class 2606 OID 16746)
-- Name: project_issue_type fk_project_issue_types; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_type
    ADD CONSTRAINT fk_project_issue_types FOREIGN KEY (project_id) REFERENCES public.project(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3133 (class 2606 OID 16741)
-- Name: project_issue_type fk_project_issue_types_colors; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_type
    ADD CONSTRAINT fk_project_issue_types_colors FOREIGN KEY (color_id) REFERENCES public.color(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3132 (class 2606 OID 16736)
-- Name: project_issue_type fk_project_issue_types_labels; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_type
    ADD CONSTRAINT fk_project_issue_types_labels FOREIGN KEY (typename_id) REFERENCES public.typename(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3172 (class 2606 OID 17225)
-- Name: project_notification fk_project_notifications_notification; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_notification
    ADD CONSTRAINT fk_project_notifications_notification FOREIGN KEY (notification_id) REFERENCES public.notification(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3171 (class 2606 OID 17220)
-- Name: project_notification fk_project_notifications_project; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_notification
    ADD CONSTRAINT fk_project_notifications_project FOREIGN KEY (project_id) REFERENCES public.project(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3173 (class 2606 OID 17236)
-- Name: project_post fk_project_posts_posts; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_post
    ADD CONSTRAINT fk_project_posts_posts FOREIGN KEY (post_id) REFERENCES public.post(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3174 (class 2606 OID 17241)
-- Name: project_post fk_project_posts_projects; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_post
    ADD CONSTRAINT fk_project_posts_projects FOREIGN KEY (project_id) REFERENCES public.project(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3140 (class 2606 OID 16771)
-- Name: project_issue_priority_type fk_project_priority_types_colors; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_priority_type
    ADD CONSTRAINT fk_project_priority_types_colors FOREIGN KEY (color_id) REFERENCES public.color(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3141 (class 2606 OID 16796)
-- Name: project_issue_priority_type fk_project_priority_types_labels; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_priority_type
    ADD CONSTRAINT fk_project_priority_types_labels FOREIGN KEY (typename_id) REFERENCES public.typename(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3139 (class 2606 OID 16766)
-- Name: project_issue_priority_type fk_project_priority_types_projects; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_issue_priority_type
    ADD CONSTRAINT fk_project_priority_types_projects FOREIGN KEY (project_id) REFERENCES public.project(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3193 (class 2606 OID 17459)
-- Name: project_team fk_project_teams_projects; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_team
    ADD CONSTRAINT fk_project_teams_projects FOREIGN KEY (project_id) REFERENCES public.project(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3194 (class 2606 OID 17464)
-- Name: project_team fk_project_teams_teams; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_team
    ADD CONSTRAINT fk_project_teams_teams FOREIGN KEY (team_id) REFERENCES public.team(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3128 (class 2606 OID 16565)
-- Name: project fk_projects_workspaces; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project
    ADD CONSTRAINT fk_projects_workspaces FOREIGN KEY (workspace_id) REFERENCES public.workspace(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3164 (class 2606 OID 17639)
-- Name: team_member fk_team_member; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team_member
    ADD CONSTRAINT fk_team_member FOREIGN KEY (user_id) REFERENCES public.workspace_member(user_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3163 (class 2606 OID 17599)
-- Name: team_member fk_team_members_team_role; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team_member
    ADD CONSTRAINT fk_team_members_team_role FOREIGN KEY (role_id) REFERENCES public.typename(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3162 (class 2606 OID 17133)
-- Name: team_member fk_team_members_teams; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team_member
    ADD CONSTRAINT fk_team_members_teams FOREIGN KEY (team_id) REFERENCES public.team(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3165 (class 2606 OID 17162)
-- Name: team_notification fk_team_notifications; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team_notification
    ADD CONSTRAINT fk_team_notifications FOREIGN KEY (notification_id) REFERENCES public.notification(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3166 (class 2606 OID 17167)
-- Name: team_notification fk_team_notifications_teams; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team_notification
    ADD CONSTRAINT fk_team_notifications_teams FOREIGN KEY (team_id) REFERENCES public.team(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3167 (class 2606 OID 17190)
-- Name: team_post fk_team_posts_posts; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team_post
    ADD CONSTRAINT fk_team_posts_posts FOREIGN KEY (post_id) REFERENCES public.post(id);


--
-- TOC entry 3168 (class 2606 OID 17195)
-- Name: team_post fk_team_posts_teams; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team_post
    ADD CONSTRAINT fk_team_posts_teams FOREIGN KEY (team_id) REFERENCES public.team(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3127 (class 2606 OID 16550)
-- Name: team fk_teams_workspaces; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.team
    ADD CONSTRAINT fk_teams_workspaces FOREIGN KEY (workspace_id) REFERENCES public.workspace(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3131 (class 2606 OID 17670)
-- Name: typename fk_typename_typename_purpose_; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.typename
    ADD CONSTRAINT fk_typename_typename_purpose_ FOREIGN KEY (purpose) REFERENCES public.typename_purpose(purpose) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3170 (class 2606 OID 17444)
-- Name: wiki_page_tag fk_wiki_page_tags_project_tags; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wiki_page_tag
    ADD CONSTRAINT fk_wiki_page_tags_project_tags FOREIGN KEY (tag_id) REFERENCES public.project_tag(typename_id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3169 (class 2606 OID 17205)
-- Name: wiki_page_tag fk_wiki_page_tags_wiki_pages; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wiki_page_tag
    ADD CONSTRAINT fk_wiki_page_tags_wiki_pages FOREIGN KEY (page_id) REFERENCES public.wiki_page(post_id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3149 (class 2606 OID 16861)
-- Name: wiki_page fk_wiki_pages_posts; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wiki_page
    ADD CONSTRAINT fk_wiki_pages_posts FOREIGN KEY (post_id) REFERENCES public.post(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3150 (class 2606 OID 16866)
-- Name: wiki_page fk_wiki_pages_projects; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.wiki_page
    ADD CONSTRAINT fk_wiki_pages_projects FOREIGN KEY (project_id) REFERENCES public.project(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3148 (class 2606 OID 16850)
-- Name: project_tag fk_wiki_tags_colors; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_tag
    ADD CONSTRAINT fk_wiki_tags_colors FOREIGN KEY (color_id) REFERENCES public.color(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3146 (class 2606 OID 16840)
-- Name: project_tag fk_wiki_tags_labels; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_tag
    ADD CONSTRAINT fk_wiki_tags_labels FOREIGN KEY (typename_id) REFERENCES public.typename(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3147 (class 2606 OID 16845)
-- Name: project_tag fk_wiki_tags_projects; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.project_tag
    ADD CONSTRAINT fk_wiki_tags_projects FOREIGN KEY (project_id) REFERENCES public.project(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3157 (class 2606 OID 17632)
-- Name: workspace_member fk_workspace_members_role; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workspace_member
    ADD CONSTRAINT fk_workspace_members_role FOREIGN KEY (role_id) REFERENCES public.typename(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3156 (class 2606 OID 16979)
-- Name: workspace_member fk_workspace_members_users; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workspace_member
    ADD CONSTRAINT fk_workspace_members_users FOREIGN KEY (user_id) REFERENCES public.member(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3155 (class 2606 OID 16974)
-- Name: workspace_member fk_workspace_members_workspace; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workspace_member
    ADD CONSTRAINT fk_workspace_members_workspace FOREIGN KEY (workspace_id) REFERENCES public.workspace(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3158 (class 2606 OID 17172)
-- Name: workspace_notification fk_workspace_notifications_nt; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workspace_notification
    ADD CONSTRAINT fk_workspace_notifications_nt FOREIGN KEY (notification_id) REFERENCES public.notification(id) ON UPDATE RESTRICT ON DELETE RESTRICT;


--
-- TOC entry 3159 (class 2606 OID 17070)
-- Name: workspace_notification fk_workspace_notifications_ws; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workspace_notification
    ADD CONSTRAINT fk_workspace_notifications_ws FOREIGN KEY (workspace_id) REFERENCES public.workspace(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3160 (class 2606 OID 17080)
-- Name: workspace_post fk_workspace_posts_posts; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workspace_post
    ADD CONSTRAINT fk_workspace_posts_posts FOREIGN KEY (post_id) REFERENCES public.post(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3161 (class 2606 OID 17085)
-- Name: workspace_post fk_workspace_posts_workspaces; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.workspace_post
    ADD CONSTRAINT fk_workspace_posts_workspaces FOREIGN KEY (workspace_id) REFERENCES public.workspace(id) ON UPDATE CASCADE ON DELETE CASCADE;


--
-- TOC entry 3342 (class 0 OID 0)
-- Dependencies: 246
-- Name: FUNCTION team_member_is_workspace_member_check(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.team_member_is_workspace_member_check() TO "Tompi";


--
-- TOC entry 3344 (class 0 OID 0)
-- Dependencies: 258
-- Name: FUNCTION typename_referential_purpose_check(); Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON FUNCTION public.typename_referential_purpose_check() TO "Tompi";


--
-- TOC entry 3346 (class 0 OID 0)
-- Dependencies: 214
-- Name: TABLE color; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.color TO "Tompi";


--
-- TOC entry 3354 (class 0 OID 0)
-- Dependencies: 216
-- Name: TABLE issue; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.issue TO "Tompi";


--
-- TOC entry 3355 (class 0 OID 0)
-- Dependencies: 238
-- Name: TABLE issue_assignee; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.issue_assignee TO "Tompi";


--
-- TOC entry 3356 (class 0 OID 0)
-- Dependencies: 237
-- Name: TABLE issue_label; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.issue_label TO "Tompi";


--
-- TOC entry 3357 (class 0 OID 0)
-- Dependencies: 241
-- Name: TABLE issue_notification; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.issue_notification TO "Tompi";


--
-- TOC entry 3359 (class 0 OID 0)
-- Dependencies: 240
-- Name: TABLE issue_post; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.issue_post TO "Tompi";


--
-- TOC entry 3360 (class 0 OID 0)
-- Dependencies: 239
-- Name: TABLE issue_resolution; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.issue_resolution TO "Tompi";


--
-- TOC entry 3364 (class 0 OID 0)
-- Dependencies: 211
-- Name: TABLE typename; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.typename TO "Tompi";


--
-- TOC entry 3367 (class 0 OID 0)
-- Dependencies: 203
-- Name: TABLE member; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.member TO "Tompi";


--
-- TOC entry 3372 (class 0 OID 0)
-- Dependencies: 220
-- Name: TABLE milestone; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.milestone TO "Tompi";


--
-- TOC entry 3373 (class 0 OID 0)
-- Dependencies: 242
-- Name: TABLE milestone_issue; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.milestone_issue TO "Tompi";


--
-- TOC entry 3374 (class 0 OID 0)
-- Dependencies: 243
-- Name: TABLE milestone_notification; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.milestone_notification TO "Tompi";


--
-- TOC entry 3376 (class 0 OID 0)
-- Dependencies: 224
-- Name: TABLE notification; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.notification TO "Tompi";


--
-- TOC entry 3381 (class 0 OID 0)
-- Dependencies: 209
-- Name: TABLE post; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.post TO "Tompi";


--
-- TOC entry 3383 (class 0 OID 0)
-- Dependencies: 235
-- Name: TABLE post_attachment; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.post_attachment TO "Tompi";


--
-- TOC entry 3384 (class 0 OID 0)
-- Dependencies: 236
-- Name: TABLE post_notification; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.post_notification TO "Tompi";


--
-- TOC entry 3388 (class 0 OID 0)
-- Dependencies: 207
-- Name: TABLE project; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.project TO "Tompi";


--
-- TOC entry 3390 (class 0 OID 0)
-- Dependencies: 217
-- Name: TABLE project_issue_priority_type; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.project_issue_priority_type TO "Tompi";


--
-- TOC entry 3392 (class 0 OID 0)
-- Dependencies: 218
-- Name: TABLE project_issue_status; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.project_issue_status TO "Tompi";


--
-- TOC entry 3394 (class 0 OID 0)
-- Dependencies: 212
-- Name: TABLE project_issue_type; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.project_issue_type TO "Tompi";


--
-- TOC entry 3395 (class 0 OID 0)
-- Dependencies: 232
-- Name: TABLE project_notification; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.project_notification TO "Tompi";


--
-- TOC entry 3396 (class 0 OID 0)
-- Dependencies: 233
-- Name: TABLE project_post; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.project_post TO "Tompi";


--
-- TOC entry 3397 (class 0 OID 0)
-- Dependencies: 221
-- Name: TABLE project_tag; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.project_tag TO "Tompi";


--
-- TOC entry 3399 (class 0 OID 0)
-- Dependencies: 244
-- Name: TABLE project_team; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.project_team TO "Tompi";


--
-- TOC entry 3405 (class 0 OID 0)
-- Dependencies: 205
-- Name: TABLE team; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.team TO "Tompi";


--
-- TOC entry 3407 (class 0 OID 0)
-- Dependencies: 228
-- Name: TABLE team_member; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.team_member TO "Tompi";


--
-- TOC entry 3409 (class 0 OID 0)
-- Dependencies: 229
-- Name: TABLE team_notification; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.team_notification TO "Tompi";


--
-- TOC entry 3410 (class 0 OID 0)
-- Dependencies: 230
-- Name: TABLE team_post; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.team_post TO "Tompi";


--
-- TOC entry 3412 (class 0 OID 0)
-- Dependencies: 245
-- Name: TABLE typename_purpose; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.typename_purpose TO "Tompi";


--
-- TOC entry 3415 (class 0 OID 0)
-- Dependencies: 222
-- Name: TABLE wiki_page; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.wiki_page TO "Tompi";


--
-- TOC entry 3416 (class 0 OID 0)
-- Dependencies: 231
-- Name: TABLE wiki_page_tag; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.wiki_page_tag TO "Tompi";


--
-- TOC entry 3421 (class 0 OID 0)
-- Dependencies: 201
-- Name: TABLE workspace; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.workspace TO "Tompi";


--
-- TOC entry 3423 (class 0 OID 0)
-- Dependencies: 225
-- Name: TABLE workspace_member; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.workspace_member TO "Tompi";


--
-- TOC entry 3425 (class 0 OID 0)
-- Dependencies: 226
-- Name: TABLE workspace_notification; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.workspace_notification TO "Tompi";


--
-- TOC entry 3427 (class 0 OID 0)
-- Dependencies: 227
-- Name: TABLE workspace_post; Type: ACL; Schema: public; Owner: postgres
--

GRANT ALL ON TABLE public.workspace_post TO "Tompi";


--
-- TOC entry 1874 (class 826 OID 17469)
-- Name: DEFAULT PRIVILEGES FOR TABLES; Type: DEFAULT ACL; Schema: public; Owner: postgres
--

ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public REVOKE ALL ON TABLES  FROM postgres;
ALTER DEFAULT PRIVILEGES FOR ROLE postgres IN SCHEMA public GRANT ALL ON TABLES  TO "Tompi";


-- Completed on 2021-05-13 00:40:49

--
-- PostgreSQL database dump complete
--

