#pragma once

#include "CRUDConcept.h"
#include "Repository.h"
#include <oatpp-postgresql/orm.hpp>
#include "dto/ColorDTOs.h"

template <class CRUDImpl>
concept Color = CRUDEntity<CRUDImpl, ColorDTO>;

class ColorPG : public PGCRUDEntity<ColorDTO, Tables::COLOR>
{
public:
	explicit ColorPG(const std::shared_ptr<oatpp::orm::Executor>& executor = nullptr);
};

static_assert(Color<ColorPG>, "ColorPG does not satisfy concept Color");
