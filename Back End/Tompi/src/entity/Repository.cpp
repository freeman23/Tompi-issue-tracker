#include "Repository.h"


QueryBuilder::QueryBuilder(oatpp::String name)
	: m_name(std::move(name))
{
}

QueryBuilder& QueryBuilder::set_query(oatpp::String query_text)
{
	m_query_text = std::move(query_text);
	return *this;
}

QueryBuilder& QueryBuilder::prepare(bool prepare)
{
	m_prepare = prepare;
	return *this;
}

QueryBuilder& QueryBuilder::add_param(oatpp::String name, oatpp::Void param)
{
	m_param_types.insert({name, param.valueType});
	m_params.insert({std::move(name), std::move(param)});
	return *this;
}

std::unordered_map<oatpp::String, oatpp::Void> QueryBuilder::get_params() const
{
	return m_params;
}

oatpp::data::share::StringTemplate QueryBuilder::parse(oatpp::orm::DbClient* db_client) const
{
	return db_client->parseQueryTemplate(m_name, m_query_text, m_param_types, m_prepare);
}

std::shared_ptr<oatpp::orm::QueryResult> QueryBuilder::execute(oatpp::orm::DbClient* db_client,
                                                               const std::shared_ptr<oatpp::orm::Connection>&
                                                               connection) const
{
	return db_client->execute(parse(db_client), m_params, connection);
}
