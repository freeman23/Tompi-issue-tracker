#pragma once

#include "CRUDConcept.h"
#include "Repository.h"
#include <oatpp-postgresql/orm.hpp>
#include "dto/MemberDTOs.h"

template <class CRUDImpl>
concept Member = CRUDEntity<CRUDImpl, MemberDTO>;

class MemberPG : public PGCRUDEntity<MemberDTO, Tables::MEMBER>
{
public:
	explicit MemberPG(const std::shared_ptr<oatpp::orm::Executor>& executor = nullptr);
};

static_assert(Member<MemberPG>, "MemberPG does not satisfy concept Member");
