#pragma once

#include "CRUDConcept.h"
#include "Repository.h"
#include <oatpp-postgresql/orm.hpp>
#include "dto/WorkspaceDTOs.h"

template <class CRUDImpl>
concept Workspace = CRUDEntity<CRUDImpl, WorkspaceDTO>;

class WorkspacePG : public PGCRUDEntity<WorkspaceDTO, Tables::WORKSPACE>
{
public:
	explicit WorkspacePG(const std::shared_ptr<oatpp::orm::Executor>& executor);
};

static_assert(Workspace<WorkspacePG>, "WorkspacePG does not satisfy concept Workspace");
