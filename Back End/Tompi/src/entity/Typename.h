#pragma once

#include "CRUDConcept.h"
#include "Repository.h"
#include <oatpp-postgresql/orm.hpp>
#include "dto/TypenameDTOs.h"

template <class CRUDImpl>
concept TypenamePurpose = CRUDEntity<CRUDImpl, TypenamePurposeDTO>;

template <class CRUDImpl>
concept Typename = CRUDEntity<CRUDImpl, TypenameDTO>;

class TypenamePurposePG : public PGCRUDEntity<TypenamePurposeDTO, Tables::TYPENAME_PURPOSE>
{
public:
	explicit TypenamePurposePG(const std::shared_ptr<oatpp::orm::Executor>& executor = nullptr);
};

class TypenamePG : public PGCRUDEntity<TypenameDTO, Tables::TYPENAME>
{
public:
	explicit TypenamePG(const std::shared_ptr<oatpp::orm::Executor>& executor = nullptr);
};

static_assert(TypenamePurpose<TypenamePurposePG>, "TypenamePurposePG does not satisfy concept TypenamePurpose");
static_assert(Typename<TypenamePG>, "TypenamePurposePG does not satisfy concept TypenamePurpose");