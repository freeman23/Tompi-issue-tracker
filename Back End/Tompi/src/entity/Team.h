#pragma once

#include "CRUDConcept.h"
#include "Repository.h"
#include <oatpp-postgresql/orm.hpp>
#include "dto/TeamDTOs.h"

template <class CRUDImpl>
concept Team = CRUDEntity<CRUDImpl, TeamDTO>;

class TeamPG : public PGCRUDEntity<TeamDTO, Tables::TEAM>
{
public:
	explicit TeamPG(const std::shared_ptr<oatpp::orm::Executor>& executor = nullptr);
};

static_assert(Team<TeamPG>, "TeamPG does not satisfy concept Team");
