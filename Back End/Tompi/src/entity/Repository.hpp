#pragma once
#include "Repository.h"
#include <oatpp/core/utils/ConversionUtils.hpp>
#include <numeric>
#include "helper/Misc.h"

template <primary_dto EntityDTO, class Func>
oatpp::Object<EntityDTO> clone_for_each_field(const oatpp::Object<EntityDTO>& dto, Func func)
{
	using poly_dispatch_ptr = const oatpp::data::mapping::type::__class::AbstractObject::PolymorphicDispatcher*;

	const auto& list = static_cast<poly_dispatch_ptr>(dto.valueType->polymorphicDispatcher)->getProperties()->getList();
	auto clone = oatpp::Object<EntityDTO>::createShared();
	auto map = static_cast<poly_dispatch_ptr>(clone.valueType->polymorphicDispatcher)->getProperties()->getMap();

	std::ranges::for_each(list, [&](const auto& field)
	{
		if (const auto value = field->get(dto.get()); field->name != EntityDTO::get_primary_key_name())
		{
			func(field);
			map[field->name]->set(clone.get(), value);
		}
	});
	return clone;
}

template <primary_dto EntityDTO>
struct filter_primary_column
{
	constexpr bool operator()(const char* name)
	{
		return name != EntityDTO::get_primary_key_name();
	}
};

template <class EntityDTO, class Filter = filter_primary_column<EntityDTO>, class Fn = just_return>
std::string get_columns(Filter predicate = Filter(), Fn func = Fn{})
{
	const std::list<oatpp::BaseObject::Property*>& properties = EntityDTO::get_fields();
	one_time_boolean is_first = true;
	return std::transform_reduce(
		properties.begin(), properties.end(), std::string{}, std::plus<std::string>{},
		[&](const auto* prop)
		{
			if (!predicate(prop->name)) return std::string{};
			return std::string{is_first ? "" : ","} + func(prop->name);
		});
}

template <primary_dto EntityDTO, Tables table>
constexpr oatpp::String PGCRUDEntity<EntityDTO, table>::query_name(const oatpp::String& action, size_t size)
{
	return action + "_" + table_name + (size == 1 ? "" : "_" + oatpp::utils::conversion::uint64ToStr(size));
}

template <primary_dto EntityDTO, Tables table>
PGCRUDEntity<EntityDTO, table>::PGCRUDEntity(const std::shared_ptr<oatpp::orm::Executor>& executor): DbClient(executor)
{
}

template <primary_dto EntityDTO, Tables table>
template <object_dto_range Range>
std::string PGCRUDEntity<EntityDTO, table>::add_values(const Range& range, QueryBuilder& query)
{
	std::string values;
	one_time_boolean is_first = true;
	size_t index = 0;
	std::ranges::for_each(range, [&](const auto& entity)
	{
		values += std::string{is_first ? "" : ","} + "(";
		is_first.reset();
		const std::string& name = "entity" + std::to_string(index++);

		auto copy_entity = clone_for_each_field(entity, [&](const auto& field)
		{
			values += std::string{is_first ? "" : ","} + ":" + name + '.' + field->name;
		});
		query.add_param(name.c_str(), copy_entity);
		values += ")";
	});
	return values;
}

template <primary_dto EntityDTO, Tables table>
template <dto_id_range Range>
std::string PGCRUDEntity<EntityDTO, table>::add_values(const Range& range, QueryBuilder& query)
{
	std::string values;
	one_time_boolean is_first = true;
	size_t index = 0;
	std::ranges::for_each(range, [&](const auto& entity)
	{
		const std::string& name = "entity" + std::to_string(index++);
		values += std::string{is_first ? "" : ","} + ":" + name;
		query.add_param(name.c_str(), entity);
	});
	return values;
}

template <primary_dto EntityDTO, Tables table>
std::shared_ptr<oatpp::orm::QueryResult> PGCRUDEntity<EntityDTO, table>::insert(
	const oatpp::Object<EntityDTO>& entity, const std::shared_ptr<oatpp::orm::Connection>& connection)
{
	return this->insert_all(std::ranges::single_view(entity), connection);
}

template <primary_dto EntityDTO, Tables table>
template <object_dto_range Range>
std::shared_ptr<oatpp::orm::QueryResult> PGCRUDEntity<EntityDTO, table>::insert_all(
	const Range& range, const std::shared_ptr<oatpp::orm::Connection>& connection)
{
	static const std::string& just_columns = get_columns<EntityDTO>();
	auto query = QueryBuilder(query_name("save_all", std::ranges::size(range)));
	const std::string& values = add_values(range, query);
	return query.set_query(std::format("INSERT INTO {0} ({1}) VALUES {2} RETURNING *;",
	                                   table_name, just_columns, values).c_str()).
	             execute(this, connection);
}

template <primary_dto EntityDTO, Tables table>
std::shared_ptr<oatpp::orm::QueryResult> PGCRUDEntity<EntityDTO, table>::update(
	const oatpp::Object<EntityDTO>& entity, const std::shared_ptr<oatpp::orm::Connection>& connection)
{
	return this->update_all(std::ranges::single_view{entity}, connection);
}

template <primary_dto EntityDTO, Tables table>
template <object_dto_range Range>
std::shared_ptr<oatpp::orm::QueryResult> PGCRUDEntity<EntityDTO, table>::update_all(
	const Range& range, const std::shared_ptr<oatpp::orm::Connection>& connection)
{
	static const std::string& source_set_columns = get_columns<EntityDTO>(
		filter_primary_column<EntityDTO>{}, [](const std::string& name) { return name + "=source." + name; });
	static const std::string& all_columns = get_columns<EntityDTO>(always_true{});

	auto query = QueryBuilder(query_name("update_all", std::ranges::size(range)));
	const std::string& values = add_values(range, query);
	return query.
	       set_query(std::format(
		       "UPDATE {0} as target SET {1} FROM (VALUES {4}) as source({2}) WHERE target.{3} = source.{3} RETURNING target.*;",
		       table_name, source_set_columns, all_columns, EntityDTO::get_primary_key_name(), values).c_str()).
	       execute(this, connection);
}

template <primary_dto EntityDTO, Tables table>
std::shared_ptr<oatpp::orm::QueryResult> PGCRUDEntity<EntityDTO, table>::count(
	const std::shared_ptr<oatpp::orm::Connection>& connection)
{
	return QueryBuilder(query_name("count")).
	       set_query(std::format("SELECT COUNT(*) FROM {0}", table_name).c_str()).
	       execute(this, connection);
}

template <primary_dto EntityDTO, Tables table>
std::shared_ptr<oatpp::orm::QueryResult> PGCRUDEntity<EntityDTO, table>::find_by_id(
	const typename EntityDTO::IdType& id, const std::shared_ptr<oatpp::orm::Connection>& connection)
{
	return this->find_all_by_ids(std::ranges::single_view{id}, connection);
}

template <primary_dto EntityDTO, Tables table>
std::shared_ptr<oatpp::orm::QueryResult> PGCRUDEntity<EntityDTO, table>::find_all(
	const std::shared_ptr<oatpp::orm::Connection>& connection)
{
	return QueryBuilder(query_name("find_all")).
	       set_query(std::format("SELECT * FROM {0}", table_name).c_str()).
	       execute(this, connection);
}

template <primary_dto EntityDTO, Tables table>
template <dto_id_range Range>
std::shared_ptr<oatpp::orm::QueryResult> PGCRUDEntity<EntityDTO, table>::find_all_by_ids(
	const Range& range, const std::shared_ptr<oatpp::orm::Connection>& connection)
{
	auto query = QueryBuilder(query_name("find_all_by_ids", std::ranges::size(range)));
	const std::string& values = add_values(range, query);
	return query.
	       set_query(std::format("SELECT * FROM {0} WHERE {1} in ({2})",
	                             table_name, EntityDTO::get_primary_key_name(), values).c_str()).
	       execute(this, connection);
}

template <primary_dto EntityDTO, Tables table>
std::shared_ptr<oatpp::orm::QueryResult> PGCRUDEntity<EntityDTO, table>::delete_by_id(
	const typename EntityDTO::IdType& id, const std::shared_ptr<oatpp::orm::Connection>& connection)
{
	return this->delete_all_by_ids(std::ranges::single_view{id}, connection);
}

template <primary_dto EntityDTO, Tables table>
template <dto_id_range Range>
std::shared_ptr<oatpp::orm::QueryResult> PGCRUDEntity<EntityDTO, table>::delete_all_by_ids(
	const Range& range, const std::shared_ptr<oatpp::orm::Connection>& connection)
{
	auto query = QueryBuilder(query_name("delete_all_by_ids", std::ranges::size(range)));
	const std::string& values = add_values(range, query);
	return query.set_query(std::format("DELETE FROM {0} WHERE {1} in ({2})",
	                                   table_name, EntityDTO::get_primary_key_name(), values).c_str()).
	             execute(this, connection);
}

template <primary_dto EntityDTO, Tables table>
std::shared_ptr<oatpp::orm::QueryResult> PGCRUDEntity<EntityDTO, table>::delete_all(
	const std::shared_ptr<oatpp::orm::Connection>& connection)
{
	return QueryBuilder(query_name("delete_all")).
	       set_query(std::format("DELETE FROM {0}", table_name).c_str()).
	       execute(this, connection);
}
