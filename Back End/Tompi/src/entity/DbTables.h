#pragma once

enum class Tables
{
	TEAM,
	COLOR,
	MEMBER,
	TYPENAME,
	WORKSPACE,
	NOTIFICATION,
	TYPENAME_PURPOSE,
};

// ReSharper disable once CppNotAllPathsReturnValue
// If the switch case is not full, i.e you forgot to add the new case,
// then a compiler error shall be generated, and you will come back and add that case.
constexpr const char* to_string(Tables e)
{
	switch (e)
	{
	case Tables::TEAM: return "team";
	case Tables::COLOR: return "color";
	case Tables::MEMBER: return "member";
	case Tables::TYPENAME: return "typename";
	case Tables::WORKSPACE: return "workspace";
	case Tables::NOTIFICATION: return "notification";
	case Tables::TYPENAME_PURPOSE: return "typename_purpose";
	}
}

