#pragma once

#include <oatpp/orm/DbClient.hpp>
#include <oatpp/core/Types.hpp>
#include "CRUDConcept.h"
#include "DbTables.h"
#include "helper/Concepts.h"

class QueryBuilder
{
private:
	const oatpp::String m_name;

	bool m_prepare = true;
	oatpp::String m_query_text;
	oatpp::orm::Executor::ParamsTypeMap m_param_types;
	std::unordered_map<oatpp::String, oatpp::Void> m_params;

public:
	QueryBuilder(oatpp::String name);

	QueryBuilder& set_query(oatpp::String query_text);

	QueryBuilder& prepare(bool prepare);

	QueryBuilder& add_param(oatpp::String name, oatpp::Void param);

	[[nodiscard]] std::unordered_map<oatpp::String, oatpp::Void> get_params() const;

	[[nodiscard]] oatpp::data::share::StringTemplate
	parse(oatpp::orm::DbClient* db_client) const;

	[[nodiscard]] std::shared_ptr<oatpp::orm::QueryResult>
	execute(oatpp::orm::DbClient* db_client,
	        const std::shared_ptr<oatpp::orm::Connection>& connection = nullptr) const;
};

template <primary_dto EntityDTO, Tables table>
class PGCRUDEntity : public oatpp::orm::DbClient
{
protected:
	constexpr static oatpp::String query_name(const oatpp::String& action, size_t size = 1);

	PGCRUDEntity(const std::shared_ptr<oatpp::orm::Executor>& executor);

	template <object_dto_range Range>
	std::string add_values(const Range& range, QueryBuilder&);
	template <dto_id_range Range>
	std::string add_values(const Range& range, QueryBuilder&);
public:
	constexpr static const char* table_name = to_string(table);

	[[nodiscard]] std::shared_ptr<oatpp::orm::QueryResult>
	insert(const oatpp::Object<EntityDTO>& entity, const std::shared_ptr<oatpp::orm::Connection>& connection = nullptr);

	template <object_dto_range Range>
	[[nodiscard]] std::shared_ptr<oatpp::orm::QueryResult>
	insert_all(const Range& range, const std::shared_ptr<oatpp::orm::Connection>& connection = nullptr);

	[[nodiscard]] std::shared_ptr<oatpp::orm::QueryResult>
	update(const oatpp::Object<EntityDTO>& entity, const std::shared_ptr<oatpp::orm::Connection>& connection = nullptr);

	template <object_dto_range Range>
	[[nodiscard]] std::shared_ptr<oatpp::orm::QueryResult>
	update_all(const Range& range, const std::shared_ptr<oatpp::orm::Connection>& connection = nullptr);
	
	[[nodiscard]] std::shared_ptr<oatpp::orm::QueryResult>
	count(const std::shared_ptr<oatpp::orm::Connection>& connection = nullptr);

	[[nodiscard]] std::shared_ptr<oatpp::orm::QueryResult>
	find_by_id(const typename EntityDTO::IdType& id, const std::shared_ptr<oatpp::orm::Connection>& connection = nullptr);

	[[nodiscard]] std::shared_ptr<oatpp::orm::QueryResult>
	find_all(const std::shared_ptr<oatpp::orm::Connection>& connection = nullptr);

	template <dto_id_range Range>
	[[nodiscard]] std::shared_ptr<oatpp::orm::QueryResult>
	find_all_by_ids(const Range& range, const std::shared_ptr<oatpp::orm::Connection>& connection = nullptr);

	[[nodiscard]] std::shared_ptr<oatpp::orm::QueryResult>
	delete_by_id(const typename EntityDTO::IdType& id,
	             const std::shared_ptr<oatpp::orm::Connection>& connection = nullptr);

	template <dto_id_range Range>
	[[nodiscard]] std::shared_ptr<oatpp::orm::QueryResult>
	delete_all_by_ids(const Range& range,
	                  const std::shared_ptr<oatpp::orm::Connection>& connection = nullptr);

	[[nodiscard]] std::shared_ptr<oatpp::orm::QueryResult>
	delete_all(const std::shared_ptr<oatpp::orm::Connection>& connection = nullptr);
};


#include "Repository.hpp"
