#pragma once
#include <concepts>
#include "helper/Archtypes.h"

namespace oatpp::orm
{
	class QueryResult;
}

template <class CRUDImpl, class DTO>
concept CRUDEntity = requires(CRUDImpl crud_impl,
                              typename DTO::IdType id,
                              oatpp::Object<DTO> dto,
                              archetypes::Input_Range<oatpp::Object<DTO>> dto_range,
                              archetypes::Input_Range<typename DTO::IdType> id_range)
{
	requires primary_dto<DTO>;

	{
		crud_impl.insert(dto)
	} -> std::same_as<std::shared_ptr<oatpp::orm::QueryResult>>;

	{
		crud_impl.insert_all(dto_range)
	} -> std::same_as<std::shared_ptr<oatpp::orm::QueryResult>>;

	{
		crud_impl.update(dto)
	} -> std::same_as<std::shared_ptr<oatpp::orm::QueryResult>>;

	{
		crud_impl.update_all(dto_range)
	} -> std::same_as<std::shared_ptr<oatpp::orm::QueryResult>>;

	{
		crud_impl.count()
	} -> std::same_as<std::shared_ptr<oatpp::orm::QueryResult>>;

	{
		crud_impl.find_by_id(id)
	} -> std::same_as<std::shared_ptr<oatpp::orm::QueryResult>>;

	{
		crud_impl.find_all()
	} -> std::same_as<std::shared_ptr<oatpp::orm::QueryResult>>;

	{
		crud_impl.find_all_by_ids(id_range)
	} -> std::same_as<std::shared_ptr<oatpp::orm::QueryResult>>;

	{
		crud_impl.delete_by_id(id)
	} -> std::same_as<std::shared_ptr<oatpp::orm::QueryResult>>;
	
	{
		crud_impl.delete_all_by_ids(id_range)
	} -> std::same_as<std::shared_ptr<oatpp::orm::QueryResult>>;
	
	{
		crud_impl.delete_all()
	} -> std::same_as<std::shared_ptr<oatpp::orm::QueryResult>>;
};
