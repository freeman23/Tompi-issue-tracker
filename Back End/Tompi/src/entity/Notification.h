#pragma once

#include "CRUDConcept.h"
#include "Repository.h"
#include <oatpp-postgresql/orm.hpp>
#include "dto/NotificationDTOs.h"

template <class CRUDImpl>
concept Notification = CRUDEntity<CRUDImpl, NotificationDTO>;

class NotificationPG : public PGCRUDEntity<NotificationDTO, Tables::NOTIFICATION>
{
public:
	explicit NotificationPG(const std::shared_ptr<oatpp::orm::Executor>& executor = nullptr);
};

static_assert(Notification<NotificationPG>, "NotificationPG does not satisfy concept Notification");
