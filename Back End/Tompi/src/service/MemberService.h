#pragma once

#include "entity/Member.h"
#include "dto/StatusDTOs.h"
#include <oatpp/web/protocol/http/Http.hpp>
#include "component/DatabaseComponent.h"


struct MemberService
{
	virtual ~MemberService() = default;
	[[nodiscard]] virtual oatpp::Object<MemberResponse> create_member(const oatpp::Object<MemberRequest>&) const = 0;
};

template <Member DBClient = MemberPG>
class MemberServicePG : public MemberService
{
private:
	std::shared_ptr<DBClient> m_database;
public:
	MemberServicePG(std::shared_ptr<DBClient> database = get_pg_db_connector<DBClient>())
	: m_database (std::move(database)) {}

	[[nodiscard]] oatpp::Object<MemberResponse> create_member(const oatpp::Object<MemberRequest>& dto) const override
	{
		auto hash_dto = oatpp::Object<MemberDTO>::createShared();
		hash_dto->username = dto->username;
		hash_dto->pass_hash = dto->password + " _hashed";
		auto db_result = m_database->insert(hash_dto);
		OATPP_ASSERT_HTTP(db_result->isSuccess(), oatpp::web::protocol::http::Status::CODE_500,
		                  db_result->getErrorMessage())

		const auto result = db_result->template fetch<oatpp::Vector<oatpp::Object<MemberRequest>>>();
		OATPP_ASSERT_HTTP(result->size() == 1, oatpp::web::protocol::http::Status::CODE_500, "Unknown error")
		auto resp = oatpp::Object<MemberResponse>::createShared();
		resp->username = result[0]->username;
		resp->token = dto->password + " token";
		return resp;
	}
};
