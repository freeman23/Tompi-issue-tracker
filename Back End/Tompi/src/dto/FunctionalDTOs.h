#pragma once

#include <oatpp/core/Types.hpp>
#include <oatpp/core/macro/codegen.hpp>

#include OATPP_CODEGEN_BEGIN(DTO)

class CountDTO : public oatpp::DTO {

	DTO_INIT(CountDTO, DTO)

	DTO_FIELD(UInt32, count);
};

#include OATPP_CODEGEN_END(DTO)