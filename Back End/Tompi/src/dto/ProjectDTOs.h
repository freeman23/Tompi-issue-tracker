#pragma once

#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/Types.hpp>

#include OATPP_CODEGEN_BEGIN(DTO)

class ProjectDTO : public oatpp::DTO
{
public:
	DTO_INIT(ProjectDTO, DTO)

	DTO_FIELD(UInt64, id);
	DTO_FIELD(UInt64, workspace_id);
	DTO_FIELD(String, name);
};

class ProjectTeam : public oatpp::DTO
{
public:
	DTO_INIT(ProjectTeam, DTO)

	DTO_FIELD(UInt64, team_id);
	DTO_FIELD(UInt64, project_id);
};

class ProjectPost : public oatpp::DTO
{
public:
	DTO_INIT(ProjectPost, DTO)

	DTO_FIELD(UInt64, post_id);
	DTO_FIELD(UInt64, project_id);
};

class ProjectNotification : public oatpp::DTO
{
public:
	DTO_INIT(ProjectNotification, DTO)

	DTO_FIELD(UInt64, notification_id);
	DTO_FIELD(UInt64, project_id);
};

class ProjectColorTypeDTO : public oatpp::DTO
{
public:
	DTO_INIT(ProjectColorTypeDTO, DTO)

	DTO_FIELD(UInt64, project_id);
	DTO_FIELD(UInt64, typename_id);
	DTO_FIELD(UInt64, color_id);
};

class WikiPageDTO : public oatpp::DTO
{
	DTO_INIT(WikiPageDTO, DTO)

	DTO_FIELD(UInt64, post_id);
	DTO_FIELD(UInt64, project_id);
	DTO_FIELD(String, title);
};

class WikiPageTagDTO : public oatpp::DTO
{
	DTO_INIT(WikiPageTagDTO, DTO)

	DTO_FIELD(UInt64, page_id);
	DTO_FIELD(UInt64, tag_id);
};
#include OATPP_CODEGEN_END(DTO)
