#pragma once

#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/Types.hpp>
#include "Common.h"

#include OATPP_CODEGEN_BEGIN(DTO)

class ColorDTO : public  oatpp::DTO
{
public:
	DTO_INIT_A(ColorDTO, DTO)

	DTO_FIELD_PRIMARY(UInt32, id);
	DTO_FIELD(String, hex_color);
};

#include OATPP_CODEGEN_END(DTO)