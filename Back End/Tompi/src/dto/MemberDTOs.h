#pragma once

#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/Types.hpp>
#include "Common.h"

#include OATPP_CODEGEN_BEGIN(DTO)

class MemberRequest : public oatpp::DTO
{
public:
	DTO_INIT(MemberRequest, DTO)
	
	DTO_FIELD(String, username);
	DTO_FIELD(String, password);
};

class MemberResponse : public oatpp::DTO
{
public:
	DTO_INIT(MemberResponse, DTO)
	
	DTO_FIELD(String, username);
	DTO_FIELD(String, token);
};

class MemberDTO : public oatpp::DTO
{
public:
	DTO_INIT_A(MemberDTO, DTO)

	DTO_FIELD_PRIMARY(UInt32,id)
	DTO_FIELD(String, username);
	DTO_FIELD(String, pass_hash);
};

#include OATPP_CODEGEN_END(DTO)