#pragma once

#define DTO_FIELD_PRIMARY(type, field) \
	DTO_FIELD(type, field);\
	using IdType = type;\
	[[nodiscard]] type get_primary_key() const {return field;}\
	void set_primary_key(const type& key) {(field) = key;}\
	constexpr static auto get_primary_key_name() {return #field;}

#define DTO_INIT_A(Derived, Parent)\
	DTO_INIT(Derived, Parent)\
	static auto get_fields() {return Z__CLASS_GET_FIELDS_MAP()->getList();}