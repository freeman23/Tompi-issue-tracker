#pragma once

#pragma once

#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/Types.hpp>
#include OATPP_CODEGEN_BEGIN(DTO)


class PostDTO : public oatpp::DTO
{
public:
	DTO_INIT(PostDTO, DTO)

	DTO_FIELD(UInt64, id);
	DTO_FIELD(UInt64, author_id);
	DTO_FIELD(UInt64, typename_id);
	DTO_FIELD(String, creation_date);
	DTO_FIELD(String, edit_date);
	DTO_FIELD(String, content);
};

class PostAttachment : public oatpp::DTO
{
public:
	DTO_INIT(PostAttachment, DTO)

	DTO_FIELD(UInt64, id);
	DTO_FIELD(UInt64, post_id);
	DTO_FIELD(String, file_name);
};

class PostNotificationDTO : public oatpp::DTO
{
public:
	DTO_INIT(PostNotificationDTO, DTO)

	DTO_FIELD(UInt64, post_id);
	DTO_FIELD(UInt64, notification_id);
};

#include OATPP_CODEGEN_END(DTO)
