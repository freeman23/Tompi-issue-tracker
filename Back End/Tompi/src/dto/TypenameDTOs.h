#pragma once

#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/Types.hpp>
#include "Common.h"

#include OATPP_CODEGEN_BEGIN(DTO)

class TypenameDTO : public oatpp::DTO
{
public:
	DTO_INIT_A(TypenameDTO, DTO)

	DTO_FIELD_PRIMARY(UInt32, id);
	DTO_FIELD(String, type_name);
	DTO_FIELD(UInt32, purpose_id);
};

class TypenamePurposeDTO : public oatpp::DTO
{
public:
	DTO_INIT_A(TypenamePurposeDTO, DTO)

	DTO_FIELD_PRIMARY(UInt32, id)
	DTO_FIELD(String, purpose);
	
};

#include OATPP_CODEGEN_END(DTO)
