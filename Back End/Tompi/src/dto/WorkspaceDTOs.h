#pragma once

#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/Types.hpp>
#include "Common.h"

#include OATPP_CODEGEN_BEGIN(DTO)

class WorkspaceDTO : public oatpp::DTO
{
public:
	DTO_INIT_A(WorkspaceDTO, DTO)

	DTO_FIELD_PRIMARY(UInt32, id)
	DTO_FIELD(String, name);
	DTO_FIELD(String, info);
	DTO_FIELD(String, logo);
};

class WorkspacePostDTO : public oatpp::DTO
{
public:
	DTO_INIT(WorkspacePostDTO, DTO)
	
	DTO_FIELD(UInt32, post_id);
	DTO_FIELD(UInt32, workspace_id);
};

class WorkspaceNotification : public oatpp::DTO
{
public:
	DTO_INIT(WorkspaceNotification, DTO)
	
	DTO_FIELD(UInt32, notification_id);
	DTO_FIELD(UInt32, workspace_id);
};

class WorkspaceMember : public oatpp::DTO
{
public:
	DTO_INIT(WorkspaceMember, DTO)

	DTO_FIELD(UInt32, workspace_id);
	DTO_FIELD(UInt32, user_id);
	DTO_FIELD(UInt32, role_id);
};

#include OATPP_CODEGEN_END(DTO)
