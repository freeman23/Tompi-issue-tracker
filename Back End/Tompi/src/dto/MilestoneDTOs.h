#pragma once

#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/Types.hpp>
#include OATPP_CODEGEN_BEGIN(DTO)



class MilestoneDTO : public oatpp::DTO
{
public:
	DTO_INIT(MilestoneDTO, DTO)

	DTO_FIELD(UInt64, id);
	DTO_FIELD(UInt64, project_id);
	DTO_FIELD(String, title);
	DTO_FIELD(String, overview);
	DTO_FIELD(String, completion_date);
	DTO_FIELD(String, creation_date);
	DTO_FIELD(String, due_date);
};

class MilestoneIssueDTO : public oatpp::DTO
{
public:
	DTO_INIT(MilestoneIssueDTO, DTO)

	DTO_FIELD(UInt64, milestone_id);
	DTO_FIELD(UInt64, issue_id);
};

class MilestoneNotificationDTO : public oatpp::DTO
{
public:
	DTO_INIT(MilestoneNotificationDTO, DTO)

	DTO_FIELD(UInt64, milestone_id);
	DTO_FIELD(UInt64, notification_id);
};

#include OATPP_CODEGEN_END(DTO)
