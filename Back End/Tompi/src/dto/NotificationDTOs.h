#pragma once

#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/Types.hpp>
#include "Common.h"

#include OATPP_CODEGEN_BEGIN(DTO)

class NotificationDTO : public  oatpp::DTO
{
public:
	DTO_INIT_A(NotificationDTO, DTO);

	DTO_FIELD_PRIMARY(UInt32, id)
	DTO_FIELD(UInt32, receiver_id);
	DTO_FIELD(UInt32, sender_id);
	DTO_FIELD(UInt32, status_id);
	DTO_FIELD(UInt32, entity_type_id);
	DTO_FIELD(UInt32, creation_date);
};

#include OATPP_CODEGEN_END(DTO)