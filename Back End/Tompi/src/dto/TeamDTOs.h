#pragma once

#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/Types.hpp>
#include "Common.h"

#include OATPP_CODEGEN_BEGIN(DTO)

class TeamDTO : public oatpp::DTO
{
public:
	DTO_INIT_A(TeamDTO, DTO)

	DTO_FIELD_PRIMARY(UInt32, id)
	DTO_FIELD(UInt32, workspace_id);
	DTO_FIELD(String, name);
	DTO_FIELD(String, info);
	DTO_FIELD(String, logo);
};

class TeamPostDTO : public oatpp::DTO
{
public:
	DTO_INIT(TeamPostDTO, DTO)
	
	DTO_FIELD(UInt32, post_id);
	DTO_FIELD(UInt32, team_id);
};

class TeamNotification : public oatpp::DTO
{
public:
	DTO_INIT(TeamNotification, DTO)
	
	DTO_FIELD(UInt32, notification_id);
	DTO_FIELD(UInt32, team_id);
};

class TeamMember : public oatpp::DTO
{
public:
	DTO_INIT(TeamMember, DTO)

	DTO_FIELD(UInt32, team_id);
	DTO_FIELD(UInt32, user_id);
	DTO_FIELD(UInt32, role_id);
};

#include OATPP_CODEGEN_END(DTO)
