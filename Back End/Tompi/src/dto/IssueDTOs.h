#pragma once

#include <oatpp/core/macro/codegen.hpp>
#include <oatpp/core/Types.hpp>

#include OATPP_CODEGEN_BEGIN(DTO)

class IssueDTO : public oatpp::DTO
{
public:
	DTO_INIT(IssueDTO, DTO)

	DTO_FIELD(UInt64, id);
	DTO_FIELD(UInt64, column_position);
	DTO_FIELD(UInt64, priority_id);
	DTO_FIELD(UInt64, project_id);
	DTO_FIELD(UInt64, status_id);
	DTO_FIELD(UInt64, type_id);
	DTO_FIELD(String, title);
	DTO_FIELD(String, creation_date);
	DTO_FIELD(String, due_date);
};

class IssuePostDTO : public oatpp::DTO
{
public:
	DTO_INIT(IssuePostDTO, DTO)
	
	DTO_FIELD(UInt64, post_id);
	DTO_FIELD(UInt64, issue_id);
	DTO_FIELD(UInt64, replied_id);
};

class IssueAssigneeDTO : public oatpp::DTO
{
public:
	DTO_INIT(IssueAssigneeDTO, DTO)
	
	DTO_FIELD(UInt64, user_id);
	DTO_FIELD(UInt64, issue_id);
	DTO_FIELD(UInt64, team_id);
};

class IssueNotification : public oatpp::DTO
{
public:
	DTO_INIT(IssueNotification, DTO)
	
	DTO_FIELD(UInt64, notification_id);
	DTO_FIELD(UInt64, issue_id);
};


class IssueLabel : public oatpp::DTO
{
public:
	DTO_INIT(IssueLabel, DTO)
	
	DTO_FIELD(UInt64, label_id);
	DTO_FIELD(UInt64, issue_id);
};

class IssueResolution : public oatpp::DTO
{
public:
	DTO_INIT(IssueResolution, DTO)
	
	DTO_FIELD(UInt64, issue_id);
	DTO_FIELD(UInt64, typename_id);
	DTO_FIELD(String, completion_date);
};
#include OATPP_CODEGEN_END(DTO)
