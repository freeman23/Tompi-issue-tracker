#pragma once

#include "dto/ConfigDTOs.h"

#include <oatpp/parser/json/mapping/ObjectMapper.hpp>
#include <oatpp/network/tcp/server/ConnectionProvider.hpp>
#include <oatpp/web/server/HttpConnectionHandler.hpp>
#include <oatpp/core/base/CommandLineArguments.hpp>
#include <oatpp/core/macro/component.hpp>

#include "helper/ErrorHandler.h"

class AppComponent
{
private:
	oatpp::base::CommandLineArguments m_cmd_args;
public:
	AppComponent(const oatpp::base::CommandLineArguments& cmd_args)
		: m_cmd_args(cmd_args) {}

	OATPP_CREATE_COMPONENT(oatpp::Object<ConfigDto>, config)([this]
	{
		const auto object_mapper = oatpp::parser::json::mapping::ObjectMapper::createShared();
		
		const auto config_text = oatpp::base::StrBuffer::loadFromFile(CONFIG_PATH);

		if (config_text)
		{
			const auto profiles = object_mapper->readFromString<oatpp::Fields<oatpp::Object<ConfigDto>>>(config_text);
			const auto* const profile_type = "dev";

			OATPP_LOGD("Server", "Loading configuration profile '%s'", profile_type)
			auto profile = profiles.getValueByKey(profile_type, nullptr);
			if (!profile) throw std::runtime_error("No configuration profile found");

			return profile;
		}
	    OATPP_LOGE("AppComponent", "Can't load configuration file at path '%s'", CONFIG_PATH)
		throw std::runtime_error("[AppComponent]: Can't load configuration file");
	}());

	OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::data::mapping::ObjectMapper>, api_object_mapper)([]
		{
			auto mapper = oatpp::parser::json::mapping::ObjectMapper::createShared();
			mapper->getSerializer()->getConfig()->useBeautifier = true;
			return mapper;
		}());

	OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::network::ServerConnectionProvider>, server_connection_provider)([]
		{
			OATPP_COMPONENT(oatpp::Object<ConfigDto>, app_config);
			return oatpp::network::tcp::server::ConnectionProvider::createShared({ app_config->host, app_config->port, oatpp::network::Address::IP_4 });
		}());


	OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::web::server::HttpRouter>, http_router)([]
		{
			return oatpp::web::server::HttpRouter::createShared();
		}());
	
	OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::network::ConnectionHandler>, server_connection_handler)([]
		{
			OATPP_COMPONENT(std::shared_ptr<oatpp::web::server::HttpRouter>, router); 
			OATPP_COMPONENT(std::shared_ptr<oatpp::data::mapping::ObjectMapper>, objectMapper);

			auto connectionHandler = oatpp::web::server::HttpConnectionHandler::createShared(router);
			connectionHandler->setErrorHandler(std::make_shared<ErrorHandler>(objectMapper));
			return connectionHandler;
		}());
};
