#pragma once

#include "dto/ConfigDTOs.h"

#include <oatpp-swagger/Model.hpp>
#include <oatpp-swagger/Resources.hpp>
#include <oatpp/core/utils/ConversionUtils.hpp>
#include <oatpp/core/macro/component.hpp>

class SwaggerComponent
{
	OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::swagger::DocumentInfo>, swagger_document_info)([]
		{
			OATPP_COMPONENT(oatpp::Object<ConfigDto>, config);

			oatpp::swagger::DocumentInfo::Builder builder;

			builder
				.setTitle("Tompi")
				.setDescription("Tompi Swagger Description")
				.setVersion("1.0")
				.addServer("http://localhost:" + oatpp::utils::conversion::int32ToStr(config->port), "serve on localhost");

			return builder.build();
		}());


	OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::swagger::Resources>, swaggerResources)([]
		{
			OATPP_COMPONENT(oatpp::Object<ConfigDto>, config);
			return oatpp::swagger::Resources::loadResources(config->swaggerRes);
		}());
};