#pragma once

#include <filesystem>

#include "dto/ConfigDTOs.h"
#include <oatpp/core/macro/component.hpp>
#include <oatpp-postgresql/orm.hpp>
#include <helper/Exec.h>

class DatabaseComponent
{
public:
	DatabaseComponent()
	{
		OATPP_COMPONENT(oatpp::Object<ConfigDto>, config);
		OATPP_LOGI("--- DatatbaseComponent","Starting schema migration with Flyway ---")
		if(exec_block(config->flyway_migrate->std_str()) != 0) throw std::runtime_error("Failed to run migrations... Aborting");
		OATPP_LOGI("--- DatatbaseComponent","Finished schema migration with Flyway ---")
	}
	
	OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::postgresql::Executor>, executor)([]
	{
		OATPP_COMPONENT(oatpp::Object<ConfigDto>, config);

		const auto connection_provider = std::make_shared<oatpp::postgresql::ConnectionProvider>(config->dbConnectionString);
		
		auto connection_pool = oatpp::postgresql::ConnectionPool::createShared(connection_provider, 10, std::chrono::seconds(5));

		return std::make_shared<oatpp::postgresql::Executor>(connection_pool);
	}());
};

template<class T>
std::shared_ptr<T> get_pg_db_connector()
{
	OATPP_COMPONENT(std::shared_ptr<oatpp::postgresql::Executor>, executor);
	return std::make_shared<T>(executor);
}
