cmake_minimum_required (VERSION 3.19)

target_include_directories(${PROJECT_NAME} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(${TARGET_TEST} PRIVATE ${CMAKE_CURRENT_SOURCE_DIR})

add_subdirectory("component")
add_subdirectory("controller")
add_subdirectory("dto")
add_subdirectory("entity")
add_subdirectory("service")
add_subdirectory("helper")