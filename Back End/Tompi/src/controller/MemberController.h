#pragma once

#include "service/MemberService.h"

#include <oatpp/web/server/api/ApiController.hpp>
#include <oatpp/core/macro/codegen.hpp>

#include OATPP_CODEGEN_BEGIN(ApiController)

class MemberController : public oatpp::web::server::api::ApiController
{
public:
	MemberController(const std::shared_ptr<ObjectMapper>& object_mapper, std::shared_ptr<MemberService> service = std::make_shared<MemberServicePG<>>())
		: ApiController(object_mapper), member_service(std::move(service)) {}

private:
	std::shared_ptr<MemberService> member_service;

public:
	static std::shared_ptr<MemberController> createShared(OATPP_COMPONENT(std::shared_ptr<ObjectMapper>, object_mapper))
	{
		return std::make_shared<MemberController>(object_mapper);
	}

	ENDPOINT("POST", "member", create_member,
		BODY_DTO(Object<MemberRequest>, member_dto))
	{
		return createDtoResponse(Status::CODE_200, member_service->create_member(member_dto));
	}
};

#include OATPP_CODEGEN_END(ApiController)