#pragma once

struct one_time_boolean
{
private:
	const bool m_value;
	mutable bool first_flag = true;
public:
	one_time_boolean(bool value) : m_value(value)
	{
	}

	operator bool() const
	{
		if (!first_flag) return !m_value;
		first_flag = false;
		return m_value;
	}

	void reset() const
	{
		first_flag = true;
	}
};

struct just_return
{
	constexpr auto operator()(const auto& val)
	{
		return val;
	}
};


struct always_true
{
	constexpr bool operator()(auto...)
	{
		return true;
	}
};

struct always_false
{
	constexpr bool operator()(auto...)
	{
		return false;
	}
};