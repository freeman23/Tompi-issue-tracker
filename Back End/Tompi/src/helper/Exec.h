#pragma once

#include <reproc++/run.hpp>
#include <string>
#include <oatpp/core/base/Environment.hpp>

namespace reproc
{
#ifdef _WIN32
	inline static const std::string& reproc_shell = "powershell";
#else
	inline static const std::string& reproc_shell = "bash";
#endif
}

inline int exec_block(const std::string& cmd) noexcept
{
	int status = -1;
	std::error_code ec;
	std::string out, err;
	reproc::sink::string out_sink(out);
	reproc::sink::string err_sink(err);

	reproc::options options;
	options.redirect.parent = true;
	options.deadline = reproc::milliseconds(15000);

	const reproc::arguments arguments{ std::vector<std::string>{reproc::reproc_shell,cmd} };

	std::tie(status, ec) = reproc::run(arguments, options, out_sink, err_sink);

	if (ec)
	{
		OATPP_LOGE(cmd, "-->:%s...", ec.message().c_str(), "-->: %s...", err.c_str())
	}
	else if (!out.empty())
	{
		OATPP_LOGI(cmd, "-->:%s...", out.c_str())
	}
	return status;
}
