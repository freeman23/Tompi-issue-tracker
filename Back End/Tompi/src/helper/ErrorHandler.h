#pragma once

#include <oatpp/web/server/handler/ErrorHandler.hpp>
#include <oatpp/web/protocol/http/outgoing/ResponseFactory.hpp>

class ErrorHandler : public oatpp::web::server::handler::ErrorHandler
{
private:
	std::shared_ptr<oatpp::data::mapping::ObjectMapper> m_object_mapper;

public:

	ErrorHandler(std::shared_ptr<oatpp::data::mapping::ObjectMapper> object_mapper);

	std::shared_ptr<oatpp::web::protocol::http::outgoing::Response>
	handleError(const oatpp::web::protocol::http::Status& status, const oatpp::String& message, const Headers& headers) override;
};