#pragma once
#include <ranges>
#include <oatpp/core/Types.hpp>

template <class Type>
concept is_dto_wrapper = std::_Is_specialization_v<Type, oatpp::data::mapping::type::DTOWrapper>
&& std::is_base_of_v<oatpp::DTO, typename Type::ObjectType>;

template <class Range>
concept object_dto_range = std::ranges::input_range<Range> &&
is_dto_wrapper<std::ranges::range_value_t<Range>>;

template <class Type>
concept dto_id_object =
(std::is_same_v<Type, oatpp::data::mapping::type::UInt32>);

template <class Range>
concept dto_id_range = std::ranges::input_range<Range> && dto_id_object<std::ranges::range_value_t<Range>>;

template <class DTO>
concept primary_dto = requires(DTO dt)
{
	requires std::is_base_of_v<oatpp::DTO, DTO>;
	{ DTO::get_primary_key_name() } -> std::convertible_to<std::string>;
	{ DTO::get_fields() } -> std::same_as<std::list<oatpp::BaseObject::Property*>>;
	{ dt.get_primary_key() } -> std::same_as<typename DTO::IdType>;
	dt.set_primary_key(typename DTO::IdType{});
};
