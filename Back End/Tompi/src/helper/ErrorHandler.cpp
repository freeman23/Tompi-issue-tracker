#include "ErrorHandler.h"
#include "dto/StatusDTOs.h"

ErrorHandler::ErrorHandler(std::shared_ptr<oatpp::data::mapping::ObjectMapper> object_mapper)
	: m_object_mapper(std::move(object_mapper))
{}

std::shared_ptr<oatpp::web::protocol::http::outgoing::Response> ErrorHandler::handleError(
	const oatpp::web::protocol::http::Status& status, const oatpp::String& message, const Headers& headers)
{
	auto error = StatusDto::createShared();
	error->status = "Error";
	error->code = status.code;
	error->message = message;

	auto resp = oatpp::web::protocol::http::outgoing::ResponseFactory::createResponse(status, error, m_object_mapper);

	for (const auto& pair : headers.getAll())
	{
		resp->putHeader(pair.first.toString(), pair.second.toString());
	}

	return resp;
}
