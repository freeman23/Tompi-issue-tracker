#pragma once

#include <oatpp/core/base/Environment.hpp>
#include <fmt/color.h>

class FmtLogger : public oatpp::base::Logger
{

public:
	void log(v_uint32 priority, const std::string& tag, const std::string& message) override
	{
		fmt::color color;
		char prefix = '\0';
		switch (priority)
		{
		case PRIORITY_V:
			color = fmt::color::gold;
			prefix = 'V';
			break;
		case PRIORITY_D:
			color = fmt::color::forest_green;
			prefix = 'D';
			break;
		case PRIORITY_I:
			color = fmt::color::royal_blue;
			prefix = 'I';
			break;
		case PRIORITY_W:
			color = fmt::color::orange_red;
			prefix = 'W';
			break;
		case PRIORITY_E:
			color = fmt::color::crimson;
			prefix = 'E';
			break;
		default:
			color = fmt::color::white;
			break;
		}

		char time_buf[50];
		struct tm now {};

		time_t seconds = std::chrono::duration_cast<std::chrono::seconds>(std::chrono::system_clock::now().time_since_epoch()).count();
		localtime_s(&now, &seconds);
		strftime(time_buf, sizeof time_buf, "%Y-%m-%d %H:%M:%S", &now);

		const auto form = format(fg(color), "{} | {} | ", prefix,time_buf) +
			format(fg(fmt::color::papaya_whip),"{} | {}\n", tag, message);
		fmt::print(form);
	}
};