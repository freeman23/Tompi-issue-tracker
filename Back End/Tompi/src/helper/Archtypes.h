// ReSharper disable CppFunctionIsNotImplemented | These functions are not intended to be linked.
#pragma once
#include "Concepts.h"
namespace archetypes
{
	// private, only used for concept definitions, NEVER in real code
	template <class T>
	class InputIterator
	{
	public:
		InputIterator();
		~InputIterator();
		InputIterator(const InputIterator& other);
		InputIterator(InputIterator&& other) noexcept;
		InputIterator& operator=(const InputIterator& other);
		InputIterator& operator=(InputIterator&& other) noexcept;


		using iterator_category = std::input_iterator_tag;
		using value_type = T;
		using reference = T&;
		using pointer = T*;
		using difference_type = std::ptrdiff_t;

		bool operator==(const InputIterator&) const;
		bool operator!=(const InputIterator&) const;
		reference operator*() const;
		InputIterator& operator++();
		InputIterator operator++(int);
	};
	
	template <class T>
	struct Input_Range
	{
		Input_Range(const Input_Range& other) = delete;
		Input_Range(Input_Range&& other) = delete;
		Input_Range& operator=(const Input_Range& other) = delete;
		Input_Range& operator=(Input_Range&& other) = delete;
		~Input_Range();

		using iterator = InputIterator<T>;

		iterator begin();
		iterator end();
	};
}
