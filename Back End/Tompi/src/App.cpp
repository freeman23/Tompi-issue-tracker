#include <iostream>
#include "helper/Exec.h"
#include "component/AppComponent.h"
#include "component/SwaggerComponent.h"
#include "component/DatabaseComponent.h"
#include <oatpp-swagger/Controller.hpp>
#include <oatpp/network/Server.hpp>
#include "controller/MemberController.h"
#include "helper/Logger.h"

void run(const oatpp::base::CommandLineArguments&);

int main(int argc, const char * argv[])
{
	oatpp::base::Environment::init(std::make_shared<FmtLogger>());
	run(oatpp::base::CommandLineArguments(argc, argv));
	oatpp::base::Environment::destroy();
	
	return 0;
}

void run(const oatpp::base::CommandLineArguments& args)
{
	AppComponent app_component(args);
	SwaggerComponent swagger_component;
	DatabaseComponent database_component;
	
	auto router = app_component.http_router.getObject();
	auto doc_endpoints = oatpp::swagger::Controller::Endpoints::createShared();

	// Create user controller
	auto member_controller = MemberController::createShared();
	member_controller->addEndpointsToRouter(router);
	doc_endpoints->pushBackAll(member_controller->getEndpoints());
	// add user controller endpoints to router
	// add user controller endpoints to documented endpoints

	auto swagger_controller = oatpp::swagger::Controller::createShared(doc_endpoints);
	swagger_controller->addEndpointsToRouter(router);

	oatpp::network::Server server(app_component.server_connection_provider.getObject(), app_component.server_connection_handler.getObject());
	OATPP_LOGI("Server", "Running on port %s...", app_component.server_connection_provider.getObject()->getProperty("port").toString()->c_str())

	server.run();
}