#include "MockEntity.h"
#include "gmock/gmock.h"
#include "gtest/gtest.h"
#include "service/MemberService.h"

using dto_container_wrapper = std::span<oatpp::Object<MemberDTO>>;
using id_container_wrapper = std::span<oatpp::UInt32>;

class MockMember
{
public:
	MOCK_METHOD(std::shared_ptr<oatpp::orm::QueryResult>,
	            insert, (const oatpp::Object<MemberDTO>&), ());

	MOCK_METHOD(std::shared_ptr<oatpp::orm::QueryResult>,
	            insert_all, (dto_container_wrapper), ());

	MOCK_METHOD(std::shared_ptr<oatpp::orm::QueryResult>,
	            update, (const oatpp::Object<MemberDTO>&), ());

	MOCK_METHOD(std::shared_ptr<oatpp::orm::QueryResult>,
	            update_all, (dto_container_wrapper), ());

	MOCK_METHOD(std::shared_ptr<oatpp::orm::QueryResult>,
	            find_by_id, (const oatpp::Any& id), ());

	MOCK_METHOD(std::shared_ptr<oatpp::orm::QueryResult>,
	            delete_by_id, (const oatpp::Any& id), ());

	MOCK_METHOD(std::shared_ptr<oatpp::orm::QueryResult>,
	            count, (), ());
	
	MOCK_METHOD(std::shared_ptr<oatpp::orm::QueryResult>,
	            find_all, (), ());
	
	MOCK_METHOD(std::shared_ptr<oatpp::orm::QueryResult>,
	            find_all_by_ids, (id_container_wrapper), ());	

	MOCK_METHOD(std::shared_ptr<oatpp::orm::QueryResult>,
	            delete_all_by_ids, (id_container_wrapper), ());
	
	MOCK_METHOD(std::shared_ptr<oatpp::orm::QueryResult>,
	            delete_all, (), ());
public:
	template <std::ranges::input_range Range>
	std::shared_ptr<oatpp::orm::QueryResult> insert_all(const Range& rng)
	{
		return this->insert_all(std::span{rng});
	}

	template <std::ranges::input_range Range>
	std::shared_ptr<oatpp::orm::QueryResult> update_all(const Range& rng)
	{
		return this->update_all(std::span{rng});
	}

	template <std::ranges::input_range Range>
	std::shared_ptr<oatpp::orm::QueryResult> find_all_by_ids(const Range& rng)
	{
		return this->find_all_by_ids(std::span{rng});
	}
	template <std::ranges::input_range Range>
	std::shared_ptr<oatpp::orm::QueryResult> delete_all_by_ids(const Range& rng)
	{
		return this->delete_all_by_ids(std::span{rng});
	}
};

TEST(test_service, test)
{
	const MockEntity<MemberServicePG<MockMember>, MockMember> mock_entity(std::make_shared<MockMember>());

	auto request = std::make_shared<MemberRequest>();
	request->username = "test_user";
	request->password = "test_pass";
	const auto request_query = mock_entity.to_query(request);

	EXPECT_CALL(*mock_entity.get_entity(), insert(testing::_))
	.WillOnce(testing::Return(request_query));

	const auto resp = mock_entity.create_member(request);

	ASSERT_EQ(request->username, resp->username);
}
