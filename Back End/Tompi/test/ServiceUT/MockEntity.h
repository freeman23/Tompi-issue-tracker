#pragma once

#include "gmock/gmock.h"
#include "oatpp/orm/QueryResult.hpp"


class MockQueryResult : public oatpp::orm::QueryResult
{
public:
	MOCK_METHOD(std::shared_ptr<oatpp::orm::Connection>, getConnection, (), (const, override));
	MOCK_METHOD(bool, isSuccess, (), (const, override));
	MOCK_METHOD(oatpp::String, getErrorMessage, (), (const, override));
	MOCK_METHOD(v_int64, getPosition, (), (const, override));
	MOCK_METHOD(v_int64, getKnownCount, (), (const, override));
	MOCK_METHOD(bool, hasMoreToFetch, (), (const, override));
	MOCK_METHOD(oatpp::Void, fetch, (const oatpp::Type* const, v_int64), (override));
};


template <class ServiceImplementation, class MockedEntityType>
class MockEntity : public ServiceImplementation
{
private:
	std::shared_ptr<MockedEntityType> m_entity;

public:
	MockEntity(const std::shared_ptr<MockedEntityType>& entity)
		: ServiceImplementation(entity), m_entity(entity)
	{
	}

	[[nodiscard]] std::shared_ptr<MockedEntityType> get_entity() const { return m_entity; }

	template <class Request>
	[[nodiscard]] std::shared_ptr<oatpp::orm::QueryResult> to_query(const std::shared_ptr<Request>& req) const
	{
		const auto query = std::make_shared<MockQueryResult>();
		auto data = oatpp::Vector<oatpp::Object<Request>>::createShared();
		data->push_back(req);
		EXPECT_CALL(*query, isSuccess).WillOnce(testing::Return(true));
		EXPECT_CALL(*query, fetch(testing::_, testing::_)).WillOnce(testing::Return(data));
		return query;
	}
};
