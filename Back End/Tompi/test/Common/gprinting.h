#pragma once
#include <ostream>

namespace oatpp::data::mapping::type
{
	inline std::ostream&operator<<(std::ostream&os,const String&str)
	{
		return os << str->c_str();
	}
}
