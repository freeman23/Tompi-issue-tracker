#pragma once

#include "TestDatabase.h"
#include <gtest/gtest.h>
#include <ranges>
#include "dto/FunctionalDTOs.h"
#include "gprinting.h"

template <class T>
oatpp::Object<T> get_test_DTO() = delete;

template <class T>
oatpp::Object<T> update_test_DTO(oatpp::Object<T>& dto, bool clone = false) = delete;

template <class T>
concept EntityPack = requires(oatpp::Object<typename T::DTO> dto)
{
	requires CRUDEntity<typename T::PGEntity, typename T::DTO>;
	update_test_DTO<typename T::DTO>(dto);
	{ get_test_DTO<typename T::DTO>() } -> std::same_as<oatpp::Object<typename T::DTO>>;
};

template <EntityPack T>
class PGCRUDEntityTest : public testing::Test
{
protected:
	const TestDatabase* db;
	const std::shared_ptr<typename T::PGEntity> entity;
	std::shared_ptr<oatpp::orm::QueryResult> query_result;
	oatpp::Object<typename T::DTO> test_entity;
private:
	const std::shared_ptr<oatpp::orm::Transaction> m_transaction;

protected:
	PGCRUDEntityTest()
		: db(TestDatabase::get_instance()),
		  entity(db->init<typename T::PGEntity>()),
		  test_entity(get_test_DTO<typename T::DTO>()),
		  m_transaction(db->get_transaction())
	{
	}

	void SetUp() override{}

	[[nodiscard]] auto get_connection() const
	{
		return m_transaction->getConnection();
	}

	void TearDown() override
	{
		query_result = m_transaction->rollback();
		ASSERT_NE(query_result, nullptr);
		ASSERT_TRUE(query_result->isSuccess()) << "Failed to rollback: " << query_result->getErrorMessage();
	}

	static void SetUpTestSuite()
	{
		TestDatabase::get_instance()->truncate_table(T::PGEntity::table_name, true);
	}
};

template <class T, primary_dto E>
struct Pack
{
	typedef T PGEntity;
	typedef E DTO;
};


TYPED_TEST_SUITE_P(PGCRUDEntityTest);

TYPED_TEST_P(PGCRUDEntityTest, CanInsertWithValidUsernameAndPassword)
{
	this->query_result = this->entity->insert(this->test_entity, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Creating the entity failed with: " << this->query_result->getErrorMessage();
	const auto saved_entity =
		this->query_result->template fetch<oatpp::Vector<oatpp::Object<typename gtest_TypeParam_::DTO>>>()[0];

	ASSERT_EQ(this->test_entity, saved_entity);
}

TYPED_TEST_P(PGCRUDEntityTest, CanInsertFromRange)
{
	auto rng = std::views::iota(0, 10)
		| std::views::transform([&](int i)
		{
			return update_test_DTO(this->test_entity);
		});
	this->query_result = this->entity->insert_all(rng, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Creating the entities failed with: " << this->query_result->getErrorMessage();
	const auto count =
		this->query_result->template fetch<oatpp::Vector<oatpp::Object<typename gtest_TypeParam_::DTO>>>()->size();

	ASSERT_EQ(10, count);
}

TYPED_TEST_P(PGCRUDEntityTest, CanUpdateById)
{
	this->query_result = this->entity->insert(this->test_entity, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Failed to create entity: " << this->query_result->getErrorMessage();

	auto saved_entity =
		this->query_result->template fetch<oatpp::Vector<oatpp::Object<typename gtest_TypeParam_::DTO>>>()[0];
	this->test_entity->set_primary_key(saved_entity->get_primary_key());
	update_test_DTO(this->test_entity);

	this->query_result = this->entity->update(this->test_entity, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Failed to update workspace: " << this->query_result->getErrorMessage();
	saved_entity =
		this->query_result->template fetch<oatpp::Vector<oatpp::Object<typename gtest_TypeParam_::DTO>>>()[0];

	ASSERT_EQ(this->test_entity, saved_entity);
}

TYPED_TEST_P(PGCRUDEntityTest, CanUpdateFromRange)
{
	auto updater = [&](oatpp::Object<typename gtest_TypeParam_::DTO> source = nullptr)
	{
		return update_test_DTO((source ? source : this->test_entity), true);
	};
	std::array<oatpp::Object<typename gtest_TypeParam_::DTO>, 10> entities;
	std::ranges::generate(entities, updater);

	this->query_result = this->entity->insert_all(entities, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Creating the entities failed with: " << this->query_result->getErrorMessage();

	auto saved_entities =
		this->query_result->template fetch<oatpp::Vector<oatpp::Object<typename gtest_TypeParam_::DTO>>>();

	std::ranges::transform(saved_entities->begin(), saved_entities->end(), entities.begin(), updater);

	this->query_result = this->entity->update_all(entities, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Failed to update entities: " << this->query_result->getErrorMessage();

	saved_entities =
		this->query_result->template fetch<oatpp::Vector<oatpp::Object<typename gtest_TypeParam_::DTO>>>();

	ASSERT_TRUE(std::ranges::equal(saved_entities->begin(),saved_entities->end(),entities.begin(),entities.end()));
}

TYPED_TEST_P(PGCRUDEntityTest, CanCount)
{
	auto rng = std::views::iota(0, 10)
		| std::views::transform([&](int i)
		{
			return update_test_DTO(this->test_entity);
		});
	this->query_result = this->entity->insert_all(rng, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Creating the entities failed with: " << this->query_result->getErrorMessage();

	this->query_result = this->entity->count(this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Counting the entities failed with: " << this->query_result->getErrorMessage();
	const auto count_wrapper = this->query_result->template fetch<oatpp::Vector<oatpp::Object<CountDTO>>>()[0];
	ASSERT_EQ(10, count_wrapper->count);
}

TYPED_TEST_P(PGCRUDEntityTest, CanGetById)
{
	this->query_result = this->entity->insert(this->test_entity, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Failed to create entity: " << this->query_result->getErrorMessage();

	auto saved_entity =
		this->query_result->template fetch<oatpp::Vector<oatpp::Object<typename gtest_TypeParam_::DTO>>>()[0];

	this->query_result = this->entity->find_by_id(saved_entity->get_primary_key(), this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Failed to get entity by id: " << this->query_result->getErrorMessage();
	saved_entity =
		this->query_result->template fetch<oatpp::Vector<oatpp::Object<typename gtest_TypeParam_::DTO>>>()[0];

	ASSERT_EQ(this->test_entity, saved_entity);
}

TYPED_TEST_P(PGCRUDEntityTest, CanGetAll)
{
	const int entity_count = 10;

	for (int _ : std::views::iota(0, entity_count))
	{
		update_test_DTO(this->test_entity);
		this->query_result = this->entity->insert(this->test_entity, this->get_connection());
		ASSERT_NE(this->query_result, nullptr);
		ASSERT_TRUE(this->query_result->isSuccess())
			<< "Creating the entity failed with: " << this->query_result->getErrorMessage();
	}

	this->query_result = this->entity->find_all(this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Selecting entities failed with: " << this->query_result->getErrorMessage();
	const auto users =
		this->query_result->template fetch<oatpp::Vector<oatpp::Object<typename gtest_TypeParam_::DTO>>>();
	ASSERT_EQ(users->size(), entity_count);
}

TYPED_TEST_P(PGCRUDEntityTest, CanGetAllByIdsFromRange)
{
	auto rng = std::views::iota(0, 10)
		| std::views::transform([&](int _)
		{
			return update_test_DTO(this->test_entity);
		});
	this->query_result = this->entity->insert_all(rng, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Creating the entities failed with: " << this->query_result->getErrorMessage();
	const auto saved_entities =
		this->query_result->template fetch<oatpp::Vector<oatpp::Object<typename gtest_TypeParam_::DTO>>>();

	auto id_rng = *saved_entities | std::views::transform([](const auto& s_e) { return s_e->get_primary_key(); });

	this->query_result = this->entity->find_all_by_ids(id_rng, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Finding the entities failed with: " << this->query_result->getErrorMessage();

	const auto count =
		this->query_result->template fetch<oatpp::Vector<oatpp::Object<typename gtest_TypeParam_::DTO>>>()->size();

	ASSERT_EQ(count, 10);
}

TYPED_TEST_P(PGCRUDEntityTest, CanDeleteById)
{
	this->query_result = this->entity->insert(this->test_entity, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Failed to entity workspace: " << this->query_result->getErrorMessage();

	const auto saved_workspace =
		this->query_result->template fetch<oatpp::Vector<oatpp::Object<typename gtest_TypeParam_::DTO>>>()[0];

	this->query_result = this->entity->delete_by_id(saved_workspace->get_primary_key(), this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Failed to get entity by id: " << this->query_result->getErrorMessage();
}

TYPED_TEST_P(PGCRUDEntityTest, CanDeleteAllByIdsFromRange)
{
	auto rng = std::views::iota(0, 10)
		| std::views::transform([&](int _)
		{
			return update_test_DTO(this->test_entity);
		});
	this->query_result = this->entity->insert_all(rng, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Creating the entities failed with: " << this->query_result->getErrorMessage();
	const auto saved_entities =
		this->query_result->template fetch<oatpp::Vector<oatpp::Object<typename gtest_TypeParam_::DTO>>>();

	auto id_rng = *saved_entities | std::views::transform([](const auto& s_e) { return s_e->get_primary_key(); });

	this->query_result = this->entity->delete_all_by_ids(id_rng, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Deleting the entities failed with: " << this->query_result->getErrorMessage();
}

TYPED_TEST_P(PGCRUDEntityTest, CanDeleteAll)
{
	auto rng = std::views::iota(0, 10)
		| std::views::transform([&](int _)
		{
			return update_test_DTO(this->test_entity);
		});
	this->query_result = this->entity->insert_all(rng, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Creating the entities failed with: " << this->query_result->getErrorMessage();

	this->query_result = this->entity->delete_all(this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(this->query_result->isSuccess())
		<< "Deleting all the entities failed with: " << this->query_result->getErrorMessage();
}

REGISTER_TYPED_TEST_SUITE_P(PGCRUDEntityTest,
                            CanInsertWithValidUsernameAndPassword,
                            CanUpdateById,
                            CanGetById,
                            CanGetAll,
                            CanDeleteById,
                            CanInsertFromRange,
                            CanUpdateFromRange,
                            CanGetAllByIdsFromRange,
                            CanDeleteAllByIdsFromRange,
                            CanCount,
                            CanDeleteAll);
