cmake_minimum_required(VERSION 3.19)

target_sources(${TARGET_TEST} PRIVATE
	"TestDatabase.h"
	"TestDatabase.cpp"
	"EntityTests.h"
	"WorkspaceEntityTests.cpp"
	"MemberEntityTests.cpp"
	"TeamEntityTests.cpp"
	"NotificationEntityTests.cpp"
	"TypenameEntityTests.cpp"
	"ColorEntityTests.cpp"
	"TypenamePurposeEntityTests.cpp")