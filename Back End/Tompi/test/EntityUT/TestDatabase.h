#pragma once

#include <oatpp/core/Types.hpp>

namespace oatpp::orm
{
	class Executor;
	class Transaction;
}

class TestDatabase
{
private:
	inline static const char* db_connection = "postgresql://Tompi:tompi@localhost:5432/test";
	inline static const char* sql_path = "migration/sql/truncate_all.sqlfun";
	std::shared_ptr<oatpp::orm::Executor> m_executor;
	TestDatabase();
	void execute_query(const oatpp::String&name,const oatpp::String&query_text) const;
public:
	static TestDatabase* get_instance(bool clean = false);
	~TestDatabase();

	void clean_db() const;
	void truncate_table(const oatpp::String&table_name,bool cascade=false) const;

	[[nodiscard]] std::shared_ptr<oatpp::orm::Transaction> get_transaction() const;

	TestDatabase(const TestDatabase& other) = delete;
	TestDatabase(TestDatabase&& other) = delete;
	TestDatabase& operator=(const TestDatabase& other) = delete;
	TestDatabase& operator=(TestDatabase&& other) = delete;

	template <class Entity>
	std::shared_ptr<Entity> init() const
	{
		return std::make_shared<Entity>(m_executor);
	}
};