#include "TestDatabase.h"
#include "entity/Typename.h"
#include "EntityTests.h"
#include <oatpp/core/utils/ConversionUtils.hpp>
#include <gtest/gtest.h>

template <>
oatpp::Object<TypenameDTO> get_test_DTO()
{
	auto dto = oatpp::Object<TypenameDTO>::createShared();
	dto->type_name = "test_typename";
	return dto;
}

template<>
oatpp::Object<TypenameDTO> update_test_DTO(oatpp::Object<TypenameDTO>&dto_source, bool clone)
{
	static int counter = 0;
	oatpp::Object<TypenameDTO> dto;
	if(clone)
		dto = oatpp::Object<TypenameDTO>::createShared();
	else
		dto = dto_source;
	
	dto->id = dto_source->id;
	dto->purpose_id = dto_source->purpose_id;
	dto->type_name = "test_typename"+ oatpp::utils::conversion::int32ToStr(counter++);

	return dto;
}

bool operator==(const TypenameDTO& m1, const TypenameDTO& m2)
{
	return m1.purpose_id == m2.purpose_id && m1.type_name == m2.type_name;
}

using TypenamePack = Pack<TypenamePG, TypenameDTO>;

template <>
void PGCRUDEntityTest<TypenamePack>::SetUp()
{
	const auto typename_purpose = this->db->init<TypenamePurposePG>();
	auto test_typename_purpose = oatpp::Object<TypenamePurposeDTO>::createShared();
	test_typename_purpose->purpose = "testing";
	this->query_result = typename_purpose->insert(test_typename_purpose, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(
		this->query_result->isSuccess()) << "Creating the workspace failed: " << this->query_result->getErrorMessage();
	test_typename_purpose = this->query_result->fetch<oatpp::Vector<oatpp::Object<TypenamePurposeDTO>>>()[0];

	this->test_entity->purpose_id = test_typename_purpose->id;
}


INSTANTIATE_TYPED_TEST_SUITE_P(PGTypenameCRUDTest, PGCRUDEntityTest, TypenamePack);