#include "TestDatabase.h"
#include <iostream>
#include <oatpp-postgresql/orm.hpp>

TestDatabase::TestDatabase()
{
	oatpp::base::Environment::init();
	const auto connection_provider = std::make_shared<oatpp::postgresql::ConnectionProvider>(db_connection);
	auto connection_pool = oatpp::postgresql::ConnectionPool::createShared(
		connection_provider, 10, std::chrono::seconds(5));
	m_executor = std::make_shared<oatpp::postgresql::Executor>(connection_pool);
}

void TestDatabase::execute_query(const oatpp::String&name,const oatpp::String&query_text) const
{
	const auto query = m_executor->parseQueryTemplate(name, query_text,{},true);
	const auto res = m_executor->execute(query,{},{},nullptr);
	if(!res->isSuccess())
		throw std::runtime_error((res->getErrorMessage() + "For query: " + query_text)->c_str());
}

void TestDatabase::clean_db() const
{
	execute_query("truncate_all",oatpp::base::StrBuffer::loadFromFile(sql_path));
}

void TestDatabase::truncate_table(const oatpp::String&table_name,bool cascade) const
{
	const auto query_text = "TRUNCATE TABLE " + table_name + (cascade ?  " CASCADE" : "");
	execute_query("truncate_this",query_text);
}

std::shared_ptr<oatpp::orm::Transaction> TestDatabase::get_transaction() const
{
	return std::make_shared<oatpp::orm::Transaction>(m_executor);
}

TestDatabase* TestDatabase::get_instance(bool clean)
{
	static auto* m_database = new TestDatabase();
	if (clean) m_database->clean_db();
	return m_database;
}

TestDatabase::~TestDatabase()
{
	m_executor.reset();
	oatpp::base::Environment::destroy();
}
