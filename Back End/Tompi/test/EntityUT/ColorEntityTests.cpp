#include "TestDatabase.h"
#include "entity/Color.h"
#include "EntityTests.h"
#include <oatpp/core/utils/ConversionUtils.hpp>
#include <gtest/gtest.h>

template <>
oatpp::Object<ColorDTO> get_test_DTO()
{
	auto dto = oatpp::Object<ColorDTO>::createShared();
	dto->hex_color = "ffffff00";
	return dto;
}

template<>
oatpp::Object<ColorDTO> update_test_DTO(oatpp::Object<ColorDTO>&dto_source, bool clone)
{
	static int counter = 0;
	oatpp::Object<ColorDTO> dto;
	if(clone)
		dto = oatpp::Object<ColorDTO>::createShared();
	else
		dto = dto_source;
	
	dto->id = dto_source->id;
	dto->hex_color = "0000" + oatpp::utils::conversion::int32ToStr(counter++);

	return dto;
}

bool operator==(const ColorDTO& m1, const ColorDTO& m2)
{
	return m1.hex_color == m2.hex_color;
}

using ColorPack = Pack<ColorPG, ColorDTO>;
INSTANTIATE_TYPED_TEST_SUITE_P(PGColorCRUDTest, PGCRUDEntityTest, ColorPack);