#include "TestDatabase.h"
#include "entity/Workspace.h"
#include "EntityTests.h"
#include <gtest/gtest.h>
#include <oatpp/core/utils/ConversionUtils.hpp>

template <>
oatpp::Object<WorkspaceDTO> get_test_DTO()
{
	auto dto = oatpp::Object<WorkspaceDTO>::createShared();
	dto->name = "test_workspace";
	dto->info = "A testy workspace";
	dto->logo = "logo.png";
	return dto;
}

template <>
oatpp::Object<WorkspaceDTO> update_test_DTO(oatpp::Object<WorkspaceDTO>& dto_source, bool clone)
{
	static int counter = 0;
	oatpp::Object<WorkspaceDTO> dto;
	if (clone)
		dto = oatpp::Object<WorkspaceDTO>::createShared();
	else
		dto = dto_source;
	
	dto->id = dto_source->id;
	dto->name = "other_name_" + oatpp::utils::conversion::int32ToStr(counter);
	dto->info = "other_info_" + oatpp::utils::conversion::int32ToStr(counter);
	dto->logo = "other_logo_" + oatpp::utils::conversion::int32ToStr(counter++);

	return dto;
}

bool operator==(const WorkspaceDTO& w1, const WorkspaceDTO& w2)
{
	return w1.name == w2.name && w1.info == w2.info && w1.logo == w2.logo;
}

using WorkspacePack = Pack<WorkspacePG, WorkspaceDTO>;
INSTANTIATE_TYPED_TEST_SUITE_P(PGWorkspaceCRUDTest, PGCRUDEntityTest, WorkspacePack);
