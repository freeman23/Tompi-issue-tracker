#include "TestDatabase.h"
#include "entity/Team.h"
#include "entity/Workspace.h"
#include "EntityTests.h"
#include <oatpp/core/utils/ConversionUtils.hpp>
#include <gtest/gtest.h>

template <>
oatpp::Object<TeamDTO> get_test_DTO()
{
	auto dto = TeamDTO::createShared();
	dto->name = "testTeamName";
	dto->info = "testTeamInfo";
	dto->logo = "testTeamLogo.png";
	return dto;
}

template <>
oatpp::Object<TeamDTO> update_test_DTO(oatpp::Object<TeamDTO>& dto_source, bool clone)
{
	static int counter = 0;
	oatpp::Object<TeamDTO> dto;
	if (clone)
		dto = oatpp::Object<TeamDTO>::createShared();
	else
		dto = dto_source;

	dto->id = dto_source->id;
	dto->workspace_id = dto_source->workspace_id;
	dto->name = "testTeamName" + oatpp::utils::conversion::int32ToStr(counter);
	dto->info = "testTeamInfo" + oatpp::utils::conversion::int32ToStr(counter);
	dto->logo = "testTeamLogo" + oatpp::utils::conversion::int32ToStr(counter++);

	return dto;
}

bool operator==(const TeamDTO& m1, const TeamDTO& m2)
{
	return m1.name == m2.name && m1.workspace_id == m2.workspace_id && m1.info == m2.info && m1.logo == m2.logo;
}


using TeamPack = Pack<TeamPG, TeamDTO>;

template <>
void PGCRUDEntityTest<TeamPack>::SetUp()
{
	const auto workspace = this->db->init<WorkspacePG>();
	auto test_workspace = oatpp::Object<WorkspaceDTO>::createShared();
	test_workspace->name = "test_workspace";
	test_workspace->info = "test_workspace";
	test_workspace->logo = "test_workspace";
	this->query_result = workspace->insert(test_workspace, this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(
		this->query_result->isSuccess()) << "Creating the workspace failed: " << this->query_result->getErrorMessage();
	test_workspace = this->query_result->fetch<oatpp::Vector<oatpp::Object<WorkspaceDTO>>>()[0];

	this->test_entity->workspace_id = test_workspace->id;
}


INSTANTIATE_TYPED_TEST_SUITE_P(PGTeamCRUDTest, PGCRUDEntityTest, TeamPack);
