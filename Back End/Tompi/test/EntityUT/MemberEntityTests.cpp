#include "TestDatabase.h"
#include "entity/Member.h"
#include "EntityTests.h"
#include <oatpp/core/utils/ConversionUtils.hpp>
#include <gtest/gtest.h>

template <>
oatpp::Object<MemberDTO> get_test_DTO()
{
	auto dto = oatpp::Object<MemberDTO>::createShared();
	dto->username = "test_username";
	dto->pass_hash = "test_password";
	return dto;
}

template<>
oatpp::Object<MemberDTO> update_test_DTO(oatpp::Object<MemberDTO>&dto_source, bool clone)
{
	static int counter = 0;
	oatpp::Object<MemberDTO> dto;
	if(clone)
		dto = oatpp::Object<MemberDTO>::createShared();
	else
		dto = dto_source;
	
	dto->id = dto_source->id;
	dto->username = "updated_username_"+ oatpp::utils::conversion::int32ToStr(counter);
	dto->pass_hash = "updated_password_"+ oatpp::utils::conversion::int32ToStr(counter++);

	return dto;
}

bool operator==(const MemberDTO& m1, const MemberDTO& m2)
{
	return m1.pass_hash == m2.pass_hash && m1.username == m2.username;
}

using MemberPack = Pack<MemberPG,MemberDTO>;
INSTANTIATE_TYPED_TEST_SUITE_P(PGMemberCRUDTest, PGCRUDEntityTest, MemberPack);