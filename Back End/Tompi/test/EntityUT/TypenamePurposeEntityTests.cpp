#include "TestDatabase.h"
#include "entity/Typename.h"
#include "EntityTests.h"
#include <oatpp/core/utils/ConversionUtils.hpp>
#include <gtest/gtest.h>

template <>
oatpp::Object<TypenamePurposeDTO> get_test_DTO()
{
	auto dto = oatpp::Object<TypenamePurposeDTO>::createShared();
	dto->purpose = "testing";
	return dto;
}

template<>
oatpp::Object<TypenamePurposeDTO> update_test_DTO(oatpp::Object<TypenamePurposeDTO>&dto_source, bool clone)
{
	static int counter = 0;
	oatpp::Object<TypenamePurposeDTO> dto;
	if(clone)
		dto = oatpp::Object<TypenamePurposeDTO>::createShared();
	else
		dto = dto_source;
	
	dto->id = dto_source->id;
	dto->purpose = "testing"+ oatpp::utils::conversion::int32ToStr(counter++);

	return dto;
}

bool operator==(const TypenamePurposeDTO& m1, const TypenamePurposeDTO& m2)
{
	return m1.purpose == m2.purpose;
}

using TypenamePurposePack = Pack<TypenamePurposePG, TypenamePurposeDTO>;
INSTANTIATE_TYPED_TEST_SUITE_P(PGTypenamePurposeCRUDTest, PGCRUDEntityTest, TypenamePurposePack);