#include "TestDatabase.h"
#include "entity/Typename.h"
#include "entity/Notification.h"
#include "EntityTests.h"
#include <oatpp/core/utils/ConversionUtils.hpp>
#include <gtest/gtest.h>

#include "entity/Member.h"

template <>
oatpp::Object<NotificationDTO> get_test_DTO()
{
	auto dto = oatpp::Object<NotificationDTO>::createShared();
	dto->creation_date = nullptr;
	return dto;
}

template<>
oatpp::Object<NotificationDTO> update_test_DTO(oatpp::Object<NotificationDTO>&dto_source, bool clone)
{
	static int counter = 0;
	oatpp::Object<NotificationDTO> dto;
	if(clone)
		dto = oatpp::Object<NotificationDTO>::createShared();
	else
		dto = dto_source;
	
	

	return dto;
}

bool operator==(const NotificationDTO& m1, const NotificationDTO& m2)
{
	return m1.entity_type_id == m2.entity_type_id && m1.creation_date == m2.creation_date && m1.receiver_id == m2.receiver_id &&
		m1.sender_id == m2.sender_id && m1.status_id == m2.status_id;
}

using NotificationPack = Pack<NotificationPG, NotificationDTO>;

template <>
void PGCRUDEntityTest<NotificationPack>::SetUp()
{
	const auto typename_purpose = this->db->init<TypenamePurposePG>();

	auto notification_status = TypenamePurposeDTO::createShared();
	auto notification_entity_type = TypenamePurposeDTO::createShared();
	notification_status->purpose = "notification_status";
	notification_entity_type->purpose = "notification_entity_type";
	this->query_result = 
		typename_purpose->insert_all(std::array{notification_status, notification_entity_type},this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(
		this->query_result->isSuccess()) << "Creating the typename_purposes failed: " << this->query_result->getErrorMessage();
	
	const auto typename_entity = this->db->init<TypenamePG>();
	auto status_t = TypenameDTO::createShared();
	auto type_t = TypenameDTO::createShared();
	const auto res_p = this->query_result->fetch<oatpp::Vector<oatpp::Object<TypenamePurposeDTO>>>();
	status_t->purpose_id =  res_p[0]->id;
	status_t->type_name = "Delivered";
	type_t->purpose_id = res_p[1]->id;
	type_t->type_name = "Test";
	this->query_result = 
		typename_entity->insert_all(std::array{status_t, type_t},this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(
		this->query_result->isSuccess()) << "Creating the type names failed: " << this->query_result->getErrorMessage();
	const auto res_t = this->query_result->fetch<oatpp::Vector<oatpp::Object<TypenameDTO>>>();
	this->test_entity->status_id = res_t[0]->id;
	this->test_entity->entity_type_id = res_t[1]->id;
	
	const auto member = this->db->init<MemberPG>();
	auto sender = MemberDTO::createShared();
	auto receiver = MemberDTO::createShared();
	sender->pass_hash = sender->username = "test_sender";
	receiver->pass_hash = receiver->username = "test_receiver";
	this->query_result = 
		member->insert_all(std::array{sender, receiver},this->get_connection());
	ASSERT_NE(this->query_result, nullptr);
	ASSERT_TRUE(
		this->query_result->isSuccess()) << "Creating the users failed: " << this->query_result->getErrorMessage();
	const auto res_m = this->query_result->fetch<oatpp::Vector<oatpp::Object<MemberDTO>>>();
	this->test_entity->sender_id = res_m[0]->id;
	this->test_entity->receiver_id = res_m[1]->id;
	
}


INSTANTIATE_TYPED_TEST_SUITE_P(PGNotificationCRUDTest, PGCRUDEntityTest, NotificationPack);