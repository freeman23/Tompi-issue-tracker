#include "MockService.h"

#include "oatpp/core/data/share/StringTemplate.hpp"
#include "oatpp/network/virtual_/client/ConnectionProvider.hpp"
#include "oatpp/network/virtual_/server/ConnectionProvider.hpp"
#include "oatpp/parser/json/mapping/ObjectMapper.hpp"
#include "oatpp/web/client/ApiClient.hpp"
#include "oatpp/web/client/HttpRequestExecutor.hpp"
#include "dto/MemberDTOs.h"


#include OATPP_CODEGEN_BEGIN(ApiClient)
class ApiTestClient : public oatpp::web::client::ApiClient
{
API_CLIENT_INIT(ApiTestClient)

	oatpp::data::share::StringTemplate parsePath(const oatpp::String& name, const oatpp::String& text)
	{
		return parsePathTemplate(name, text);
	}

	static void add_headers(oatpp::web::protocol::http::Headers& headers, ...)
	{
		(void)headers;
	}
};
#include OATPP_CODEGEN_END(ApiClient)

class TestComponent
{
	/**
	 * Create oatpp virtual network interface for test networking
	 */
	OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::network::virtual_::Interface>, virtual_interface)([]
	{
		return oatpp::network::virtual_::Interface::obtainShared("virtualhost");
	}());

	/**
	 * Create server ConnectionProvider of oatpp virtual connections for test
	 */
	OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::network::ServerConnectionProvider>, server_connection_provider)([]
	{
		OATPP_COMPONENT(std::shared_ptr<oatpp::network::virtual_::Interface>, interface);
		return oatpp::network::virtual_::server::ConnectionProvider::createShared(interface);
	}());

	/**
	 * Create client ConnectionProvider of oatpp virtual connections for test
	 */
	OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::network::ClientConnectionProvider>, client_connection_provider)([]
	{
		OATPP_COMPONENT(std::shared_ptr<oatpp::network::virtual_::Interface>, interface);
		return oatpp::network::virtual_::client::ConnectionProvider::createShared(interface);
	}());

	/**
	 *  Create Router component
	 */
	OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::web::server::HttpRouter>, http_router)([]
	{
		return oatpp::web::server::HttpRouter::createShared();
	}());

	/**
	 *  Create ConnectionHandler component which uses Router component to route requests
	 */
	OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::network::ConnectionHandler>, server_connection_handler)([]
	{
		OATPP_COMPONENT(std::shared_ptr<oatpp::web::server::HttpRouter>, router);
		return oatpp::web::server::HttpConnectionHandler::createShared(router);
	}());

	/**
	 *  Create ObjectMapper component to serialize/deserialize DTOs in Controller's API
	 */
	OATPP_CREATE_COMPONENT(std::shared_ptr<oatpp::data::mapping::ObjectMapper>, api_object_mapper)([]
	{
		return oatpp::parser::json::mapping::ObjectMapper::createShared();
	}());
};

MockService::MockService()
{
	oatpp::base::Environment::init();
	test_component = std::make_unique<TestComponent>();
	m_runner = std::make_unique<oatpp::test::web::ClientServerTestRunner>();
}

MockService::~MockService()
{
	m_runner.reset();
	test_component.reset();
	oatpp::base::Environment::destroy();
}

std::shared_ptr<oatpp::data::mapping::ObjectMapper> MockService::get_object_mapper() const
{
	OATPP_COMPONENT(std::shared_ptr<oatpp::data::mapping::ObjectMapper>, api_object_mapper);
	return api_object_mapper;
}

std::shared_ptr<oatpp::web::protocol::http::incoming::Response> MockService::get(const oatpp::String& path) const
{
	OATPP_COMPONENT(std::shared_ptr<oatpp::network::ClientConnectionProvider>, client_connection_provider);
	OATPP_COMPONENT(std::shared_ptr<oatpp::data::mapping::ObjectMapper>, api_object_mapper);

	const auto request_executor = oatpp::web::client::HttpRequestExecutor::createShared(client_connection_provider);
	const auto client = ApiTestClient::createShared(request_executor, api_object_mapper);
	oatpp::web::client::ApiClient::Headers headers;
	client->add_headers(headers);
	const std::shared_ptr<oatpp::web::protocol::http::outgoing::Body> body;
	std::shared_ptr<oatpp::web::protocol::http::incoming::Response> resp;

	m_runner->run([&]
	{
		resp = client->executeRequest("GET", client->parsePath("get" + path, path), headers, {}, {}, body, nullptr);
	});

	return resp;
}

std::shared_ptr<oatpp::web::protocol::http::incoming::Response> MockService::post(const oatpp::String& path,
	const oatpp::Object<MemberRequest>& request) const
{
	OATPP_COMPONENT(std::shared_ptr<oatpp::network::ClientConnectionProvider>, client_connection_provider);
	OATPP_COMPONENT(std::shared_ptr<oatpp::data::mapping::ObjectMapper>, api_object_mapper);
	const auto request_executor = oatpp::web::client::HttpRequestExecutor::createShared(client_connection_provider);
	const auto client = ApiTestClient::createShared(request_executor, api_object_mapper);

	oatpp::web::client::ApiClient::Headers headers;
	client->add_headers(headers);
	std::unordered_map<oatpp::String, oatpp::String> path_params;
	std::unordered_map<oatpp::String, oatpp::String> query_params;
	std::shared_ptr<oatpp::web::protocol::http::incoming::Response> resp;
	std::shared_ptr<oatpp::web::protocol::http::outgoing::Body> body =
		oatpp::web::protocol::http::outgoing::BufferBody::createShared(
			api_object_mapper->writeToString(request), api_object_mapper->getInfo().http_content_type);

	m_runner->run([&]
	{
		resp = client->executeRequest("POST", client->parsePath("post" + path, path), headers, path_params,
		                              query_params, body, nullptr);
	});

	return resp;
}
