#pragma once

#include "oatpp/web/protocol/http/incoming/Response.hpp"
#include "oatpp-test/web/ClientServerTestRunner.hpp"

class MemberRequest;
class TestComponent;

class MockService
{
private:
	std::unique_ptr<TestComponent> test_component;
	std::unique_ptr<oatpp::test::web::ClientServerTestRunner> m_runner;

public:
	MockService();
	~MockService();
	MockService(const MockService& other) = delete;
	MockService(MockService&& other) = delete;
	MockService& operator=(const MockService& other) = delete;
	MockService& operator=(MockService&& other) = delete;

	template <class ApiController, class ApiControllerService>
	void controller_service(const std::shared_ptr<ApiControllerService>& service);
	[[nodiscard]] std::shared_ptr<oatpp::data::mapping::ObjectMapper> get_object_mapper() const;
	[[nodiscard]] std::shared_ptr<oatpp::web::protocol::http::incoming::Response> get(const oatpp::String& path) const;
	[[nodiscard]] std::shared_ptr<oatpp::web::protocol::http::incoming::Response> post(
		const oatpp::String& path, const oatpp::Object<MemberRequest>& request) const;
};

template <class ApiController, class ApiControllerService>
void MockService::controller_service(const std::shared_ptr<ApiControllerService>& service)
{
	OATPP_COMPONENT(std::shared_ptr<oatpp::data::mapping::ObjectMapper>, api_object_mapper);
	m_runner->addController(std::make_shared<ApiController>(api_object_mapper, service));
}
