#include "MockService.h"
#include "controller/MemberController.h"
#include "gmock/gmock.h"

class MockMemberService : public MemberService
{
public:
	MOCK_METHOD(oatpp::Object<MemberResponse>, create_member, (const oatpp::Object<MemberRequest>&), (const, override));
};


TEST(MemberControllerTest, CanCreateMember)
{
	MockService mock_mvc;
	const auto service = std::make_shared<MockMemberService>();
	mock_mvc.controller_service<MemberController>(service);
	auto req = oatpp::Object<MemberRequest>::createShared();
	req->username = "test_user";
	req->password = "test_pass";
	auto resp = oatpp::Object<MemberResponse>::createShared();
	resp->username = "test_user";

	EXPECT_CALL(*service, create_member(testing::_)).WillOnce(testing::Return(resp));

	const auto response = mock_mvc.post("/member", req);
	const auto dto = response->readBodyToDto<oatpp::Object<MemberResponse>>(mock_mvc.get_object_mapper().get());
	EXPECT_EQ(resp->username, dto->username);
}
